import {
  SymbolLatestResponse,
  SymbolPeriodResponse,
  SymbolQuoteResponse,
  MarketSessionStatusResponse,
  SymbolQuoteMinuteResponse,
  SymbolQuoteTickResponse,
  EtfNavDailyResponse,
  EtfIndexDailyResponse,
  ForeignerDailyResponse,
  PutthroughDealResponse,
  StockRankingTradeResponse,
  PutthroughAdvertiseResponse,
  SymbolStaticInfoResponse,
  IndexStockListResponse,
  FixSecurityListQueryResponse,
  TradingViewHistoryResponse,
  TradingViewSymbolSearchResponse,
  StockRankingTopResponse,
  TopForeignerTradingResponse,
  SymbolTickSizeMatchResponse,
	TopAiRatingResponse,
} from 'tradex-models-market';
import { IBidOfferItem, ISymbolInfo } from '../models/db/ISymbolInfo';
import { Utils } from 'tradex-common';
import { ISymbolDaily } from '../models/db/ISymbolDaily';
import { ISymbolQuote } from '../models/db/ISymbolQuote';
import { IMarketSessionStatus } from '../models/db/IMarketSessionStatus';
import {
  MarketTypeEnum,
  MarketSessionStatusEnum,
  MarketSessionTypeEnum,
  SecuritiesTypeEnum,
  MatchByTypeEnum,
  BaseCodeSecuritiesTypeEnum,
  StatusResponseEnum,
  TopSortTypeEnum,
  UpDownTypeEnum,
} from '../constants';
import { ISymbolQuoteMinutes } from '../models/db/ISymbolQuoteMinutes';
import { IForeignerDaily } from '../models/db/IForeignerDaily';
import { IEtfNavDaily } from '../models/db/IEtfNavDaily';
import { IDealNoticeData } from '../models/db/IDealNoticeData';
import { IAdvertiseData } from '../models/db/IAdvertiseData';
import { IIndexStockList } from '../models/db/IIndexStockList';
import { IChart } from '../models/db/IChart';
import { ISymbolWeeklyOrMonthly } from '../models/db/ISymbolWeeklyOrMonthly';
import { ISymbolPrevious } from '../models/db/ISymbolPrevious';
import { ITopAiRating } from '../models/db/ITopAiRating';

const toSymbolLatestResponse = (symbolInfo: ISymbolInfo): SymbolLatestResponse => {
  const response: SymbolLatestResponse = {};
  response.s = symbolInfo.code;
  response.t = SecuritiesTypeEnum[symbolInfo.type];
  response.o = Utils.round(symbolInfo.open);
  response.h = Utils.round(symbolInfo.high);
  response.l = Utils.round(symbolInfo.low);
  response.c = Utils.round(symbolInfo.last);
  response.a = Utils.round(symbolInfo.averagePrice);
  response.ch = Utils.round(symbolInfo.change);
  response.ra = Utils.round(symbolInfo.rate);
  response.vo = Utils.round(symbolInfo.tradingVolume);
  response.va = Utils.round(symbolInfo.tradingValue);

  if (symbolInfo.type !== SecuritiesTypeEnum.INDEX) {
    response.mv = Utils.round(symbolInfo.matchingVolume);
    response.mb = MatchByTypeEnum[symbolInfo.matchBy];
    if (symbolInfo.totalBidVolume != null) {
      response.tb = Utils.round(symbolInfo.totalBidVolume);
    }
    if (symbolInfo.totalOfferVolume != null) {
      response.to = Utils.round(symbolInfo.totalOfferVolume);
    }
    response.ss = symbolInfo.sessions;
    response.w52 = {
      h: Utils.round(symbolInfo.highPrice52Week),
      l: Utils.round(symbolInfo.lowPrice52Week),
    };

    response.bb = [];
    response.bo = [];

    if (symbolInfo.bidOfferList != null) {
      symbolInfo.bidOfferList.forEach((bidOfferItem: IBidOfferItem) => {
        response.bb.push({
          p: Utils.round(bidOfferItem.bidPrice),
          v: Utils.round(bidOfferItem.bidVolume),
          c: Utils.round(bidOfferItem.bidVolumeChange),
        });
        response.bo.push({
          p: Utils.round(bidOfferItem.offerPrice),
          v: Utils.round(bidOfferItem.offerVolume),
          c: Utils.round(bidOfferItem.offerVolumeChange),
        });
      });
    }
    response.fr = {
      bv: Utils.round(symbolInfo.foreignerBuyVolume),
      sv: Utils.round(symbolInfo.foreignerSellVolume),
      tr: Utils.round(symbolInfo.foreignerTotalRoom),
      cr: Utils.round(symbolInfo.foreignerCurrentRoom),
    };
  } else {
    response.ic = {
      ce: symbolInfo.ceilingCount,
      fl: symbolInfo.floorCount,
      up: symbolInfo.upCount,
      dw: symbolInfo.downCount,
      uc: symbolInfo.unchangedCount,
    };
  }
  if (symbolInfo.type !== SecuritiesTypeEnum.FUTURES) {
    response.oi = symbolInfo.openInterest;
    response.ba = Utils.round(symbolInfo.basis);
  }
  response.exp = symbolInfo.exercisePrice;
  response.ep = symbolInfo.expectedPrice;
  return response;
};

const toSymbolStaticInfoResponse = (symbolInfo: ISymbolInfo): SymbolStaticInfoResponse => {
  const response: SymbolStaticInfoResponse = {};
  response.s = symbolInfo.code;
  response.t = SecuritiesTypeEnum[symbolInfo.type];
  response.n1 = symbolInfo.name;
  response.n2 = symbolInfo.nameEn;
  response.m = MarketTypeEnum[symbolInfo.marketType];
  response.re = Utils.round(symbolInfo.referencePrice);
  response.ce = Utils.round(symbolInfo.ceilingPrice);
  response.fl = Utils.round(symbolInfo.floorPrice);
  if (symbolInfo.firstTradingDate != null) {
    response.ftd = Utils.formatDateToDisplay(symbolInfo.firstTradingDate);
  }
  if (symbolInfo.lastTradingDate != null) {
    response.ltd = Utils.formatDateToDisplay(symbolInfo.lastTradingDate);
  }
  if (symbolInfo.maturityDate != null) {
    response.md = Utils.formatDateToDisplay(symbolInfo.maturityDate);
  }
  response.ud = symbolInfo.underlyingSymbol;
  response.b = symbolInfo.baseCode;
  response.bs = BaseCodeSecuritiesTypeEnum[symbolInfo.baseCodeSecuritiesType];
  response.i = symbolInfo.isHighlight === 1000;

  if (symbolInfo.type !== SecuritiesTypeEnum.INDEX) {
    response.mv = Utils.round(symbolInfo.matchingVolume);
    response.mb = MatchByTypeEnum[symbolInfo.matchBy];
    if (symbolInfo.totalBidVolume != null) {
      response.tb = Utils.round(symbolInfo.totalBidVolume);
    }
    if (symbolInfo.totalOfferVolume != null) {
      response.to = Utils.round(symbolInfo.totalOfferVolume);
    }
    response.ss = symbolInfo.sessions;

    response.bb = [];
    response.bo = [];

    if (symbolInfo.bidOfferList != null) {
      symbolInfo.bidOfferList.forEach((bidOfferItem: IBidOfferItem) => {
        response.bb.push({
          p: Utils.round(bidOfferItem.bidPrice),
          v: Utils.round(bidOfferItem.bidVolume),
          c: Utils.round(bidOfferItem.bidVolumeChange),
        });
        response.bo.push({
          p: Utils.round(bidOfferItem.offerPrice),
          v: Utils.round(bidOfferItem.offerVolume),
          c: Utils.round(bidOfferItem.offerVolumeChange),
        });
      });
    }
  } else {
    response.ic = {
      ce: symbolInfo.ceilingCount,
      fl: symbolInfo.floorCount,
      up: symbolInfo.upCount,
      dw: symbolInfo.downCount,
      uc: symbolInfo.unchangedCount,
    };
  }
  return response;
};

const toSymbolDailyResponse = (symbolDaily: ISymbolDaily): SymbolPeriodResponse => {
  const response: SymbolPeriodResponse = {};
  response.o = Utils.round(symbolDaily.open === 0 ? symbolDaily.last : symbolDaily.open);
  response.h = Utils.round(symbolDaily.high === 0 ? symbolDaily.last : symbolDaily.high);
  response.l = Utils.round(symbolDaily.low === 0 ? symbolDaily.last : symbolDaily.low);
  response.c = Utils.round(symbolDaily.last);
  response.ch = Utils.round(symbolDaily.change);
  response.ra = Utils.round(symbolDaily.rate);
  response.vo = Utils.round(symbolDaily.tradingVolume);
  response.va = Utils.round(symbolDaily.tradingValue);
  response.d = Utils.formatDateToDisplay(symbolDaily.date);
  return response;
};
const toSymbolWeeklyOrMonthlyResponse = (symbolWeekly: ISymbolWeeklyOrMonthly): SymbolPeriodResponse => {
  const response: SymbolPeriodResponse = {};
  response.o = Utils.round(symbolWeekly.open === 0 ? symbolWeekly.last : symbolWeekly.open);
  response.h = Utils.round(symbolWeekly.high === 0 ? symbolWeekly.last : symbolWeekly.high);
  response.l = Utils.round(symbolWeekly.low === 0 ? symbolWeekly.last : symbolWeekly.low);
  response.c = Utils.round(symbolWeekly.last);
  response.ch = Utils.round(symbolWeekly.change);
  response.ra = Utils.round(symbolWeekly.rate);
  response.vo = Utils.round(symbolWeekly.tradingVolume);
  response.va = Utils.round(symbolWeekly.tradingValue);
  response.d = Utils.formatDateToDisplay(symbolWeekly.date);
  response.dc = symbolWeekly.dayCount;
  return response;
};
const toSymbolQuoteMinutesResponse = (
  symbolQuoteMinutes: ISymbolQuoteMinutes,
  minuteUnit: number
): SymbolQuoteMinuteResponse => {
  const response: SymbolQuoteMinuteResponse = {};
  const date = symbolQuoteMinutes.date;
  date.setSeconds(0);
  date.setMinutes(Math.trunc(date.getMinutes() / minuteUnit) * minuteUnit);
  response.l = Utils.round(symbolQuoteMinutes.low);
  response.c = Utils.round(symbolQuoteMinutes.last);
  response.h = Utils.round(symbolQuoteMinutes.high);
  response.o = Utils.round(symbolQuoteMinutes.open);
  response.pv = Utils.round(symbolQuoteMinutes.periodTradingVolume);
  response.t = Utils.formatDateToDisplay(date, Utils.DATETIME_DISPLAY_FORMAT);
  return response;
};
const toSymbolQuoteResponse = (symbolQuote: ISymbolQuote): SymbolQuoteResponse => {
  const response: SymbolQuoteResponse = {};

  response.o = Utils.round(symbolQuote.open);
  response.t = symbolQuote.time;
  response.c = Utils.round(symbolQuote.last);
  response.ch = Utils.round(symbolQuote.change);
  response.h = Utils.round(symbolQuote.high);
  response.l = Utils.round(symbolQuote.low);
  response.mb = symbolQuote.matchedBy;
  response.mv = symbolQuote.matchingVolume;
  response.ra = Utils.round(symbolQuote.rate);
  response.se = Utils.round(symbolQuote.sequence);
  response.va = Utils.round(symbolQuote.tradingValue);
  response.vo = Utils.round(symbolQuote.tradingVolume);
  response.cf = symbolQuote.ceilingFloorEqual;
  return response;
};
const toSymbolQuoteTickResponse = (
  symbolQuoteTick: ISymbolQuote,
  periodTradingVolume: number
): SymbolQuoteTickResponse => {
  const response: SymbolQuoteTickResponse = {};
  const time = symbolQuoteTick.date;
  time.setMilliseconds(0);
  response.t = time.getTime() / 1000;
  response.o = Utils.round(symbolQuoteTick.open);
  response.h = Utils.round(symbolQuoteTick.high);
  response.l = Utils.round(symbolQuoteTick.low);
  response.c = Utils.round(symbolQuoteTick.last);
  response.pv = Utils.round(periodTradingVolume);
  return response;
};
const toMarketSessionStatusResponse = (
  marketSessionStatus: IMarketSessionStatus,
  symbolPrevious: ISymbolPrevious
): MarketSessionStatusResponse => {
  if (marketSessionStatus != null) {
    const response: MarketSessionStatusResponse = {};
    response.market = MarketTypeEnum[marketSessionStatus.market];
    response.time = marketSessionStatus.time;
    response.status = MarketSessionStatusEnum[marketSessionStatus.status];
    response.type = MarketSessionTypeEnum[marketSessionStatus.type];
    if (symbolPrevious != null) {
      response.lastTradingDate = Utils.formatDateToDisplay(symbolPrevious.lastTradingDate);
      if (symbolPrevious.previousTradingDate != null) {
        response.previousTradingDate = Utils.formatDateToDisplay(symbolPrevious.previousTradingDate);
      }
    }
    return response;
  }
  return null;
};

const toEtfIndexDailyResponse = (etfNavDaily: IEtfNavDaily): EtfIndexDailyResponse => {
  return {
    cd: etfNavDaily.code,
    c: Utils.round(etfNavDaily.last),
    o: Utils.round(etfNavDaily.open),
    h: Utils.round(etfNavDaily.high),
    l: Utils.round(etfNavDaily.low),
    ch: Utils.round(etfNavDaily.change),
    r: Utils.round(etfNavDaily.rate),
    d: Utils.formatDateToDisplay(etfNavDaily.date),
  };
};

const toEtfNavDailyResponse = (etfNavDaily: IEtfNavDaily): EtfNavDailyResponse => {
  return {
    cd: etfNavDaily.code,
    c: Utils.round(etfNavDaily.last),
    o: Utils.round(etfNavDaily.open),
    h: Utils.round(etfNavDaily.high),
    l: Utils.round(etfNavDaily.low),
    ch: Utils.round(etfNavDaily.change),
    r: Utils.round(etfNavDaily.rate),
    d: Utils.formatDateToDisplay(etfNavDaily.date),
  };
};

const toForeignerDailyResponse = (symbolForeignerDaily: IForeignerDaily): ForeignerDailyResponse => {
  const response: ForeignerDailyResponse = {};
  response.br = Utils.round(symbolForeignerDaily.foreignerBuyAbleRatio);
  response.bv = Utils.round(symbolForeignerDaily.foreignerBuyVolume);
  response.cr = Utils.round(symbolForeignerDaily.foreignerCurrentRoom);
  response.cv = Utils.round(symbolForeignerDaily.foreignerChangeVolume);
  response.hr = Utils.round(symbolForeignerDaily.foreignerHoldRatio);
  response.hv = Utils.round(symbolForeignerDaily.foreignerHoldVolume);
  response.sv = Utils.round(symbolForeignerDaily.foreignerSellVolume);
  response.tr = Utils.round(symbolForeignerDaily.foreignerTotalRoom);
  return response;
};

const toPutthroughAdvertiseResponse = (advertiseData: IAdvertiseData): PutthroughAdvertiseResponse => {
  return {
    code: advertiseData.code,
    time: advertiseData.time.replace(/:/g, ''),
    secId: advertiseData.secId,
    traderId: advertiseData.traderId,
    sellBuyType: advertiseData.sellBuyType,
    price: Utils.round(advertiseData.price),
    quantity: Utils.round(advertiseData.quantity),
    ptVolume: Utils.round(advertiseData.ptVolume),
    ptValue: Utils.round(advertiseData.ptValue),
    contact: advertiseData.contact,
    isCancel: advertiseData.isCancel,
  };
};

const toPutthroughDealResponse = (dealNoticeData: IDealNoticeData): PutthroughDealResponse => {
  return {
    code: dealNoticeData.code,
    time: dealNoticeData.time.replace(/:/g, ''),
    confirmNumber: dealNoticeData.confirmNumber,
    matchPrice: Utils.round(dealNoticeData.matchPrice),
    matchVolume: Utils.round(dealNoticeData.matchVolume),
    ptVolume: Utils.round(dealNoticeData.ptVolume),
    ptValue: Utils.round(dealNoticeData.ptValue),
    isCancel: dealNoticeData.isCancel,
  };
};

const toStockRankingTradeResponse = (symbolStockRankingTrade: ISymbolInfo): StockRankingTradeResponse => {
  const response: StockRankingTradeResponse = {};
  response.c = symbolStockRankingTrade.code;
  response.cn = Utils.round(symbolStockRankingTrade.change);
  response.l = Utils.round(symbolStockRankingTrade.last);
  response.r = Utils.round(symbolStockRankingTrade.rate);
  response.to = Utils.round(symbolStockRankingTrade.turnoverRate);
  response.tr = Utils.round(symbolStockRankingTrade.tradingValue);
  response.tv = Utils.round(symbolStockRankingTrade.totalTradingVolume);
  return response;
};

const toIndexStockListResponse = (indexStockList: IIndexStockList): IndexStockListResponse => {
  return indexStockList.stockList;
};

const parseFromSymbolInfoToFixSymbol = (symbolInfo: ISymbolInfo): FixSecurityListQueryResponse => {
  return {
    ic: symbolInfo.code,
    cc: symbolInfo.cfiCode,
    c: symbolInfo.currency,
    se: symbolInfo.securityExchange,
    sd: symbolInfo.name,
    rl: symbolInfo.roundLot,
    mtv: symbolInfo.minTradeVolume,
    cm: symbolInfo.contractMultiplier,
    mmy: symbolInfo.maturityDate != null ? Utils.rightPad(`${symbolInfo.maturityDate}`, 8, '0').substring(0, 4) : null,
    md: symbolInfo.maturityDate != null ? symbolInfo.maturityDate.toString() : null,
    st: symbolInfo.type,
    us: symbolInfo.underlyingSymbol,
    cp: symbolInfo.ceilingPrice,
    fp: symbolInfo.floorPrice,
    exp: symbolInfo.exercisePrice,
    ep: symbolInfo.expectedPrice,
    er: symbolInfo.exerciseRatio,
    bc: symbolInfo.baseCodeSecuritiesType,
  };
};

const parseSymbolQuoteMinuteList = (
  symbolQuoteMinuteList: ISymbolQuoteMinutes[],
  nextTime: number = null
): TradingViewHistoryResponse => {
  const t = [];
  const o = [];
  const h = [];
  const l = [];
  const c = [];
  const v = [];
  let s = StatusResponseEnum.NO_DATA.valueOf();
  if (symbolQuoteMinuteList.length > 0) {
    const sortedList = symbolQuoteMinuteList.sort((a: ISymbolQuoteMinutes, b: ISymbolQuoteMinutes) => {
      return +a.date - +b.date;
    });
    for (const item of sortedList) {
      item.date.setSeconds(0);
      item.date.setMilliseconds(0);
      t.push(item.date.getTime() / 1000);
      o.push(Utils.round(item.open));
      h.push(Utils.round(item.high));
      l.push(Utils.round(item.low));
      c.push(Utils.round(item.last));
      v.push(Utils.round(item.periodTradingVolume));
    }
    s = StatusResponseEnum.OK.valueOf();
  }
  return {
    t: t,
    o: o,
    h: h,
    l: l,
    c: c,
    v: v,
    s: s,
    nextTime: nextTime,
  };
};

const parseTradingviewDailyPeriodList = (
  symbolPeriodResponseList: SymbolPeriodResponse[]
): TradingViewHistoryResponse => {
  const t = [];
  const o = [];
  const h = [];
  const l = [];
  const c = [];
  const v = [];
  let s = StatusResponseEnum.NO_DATA.valueOf();
  if (symbolPeriodResponseList.length > 0) {
    const sortedList = symbolPeriodResponseList.sort((a: SymbolPeriodResponse, b: SymbolPeriodResponse) => {
      return +a.d - +b.d;
    });
    for (const item of sortedList) {
      const date = Utils.convertStringToDate(item.d, Utils.DATE_DISPLAY_FORMAT);
      t.push(date.getTime() / 1000);
      o.push(item.o > 0 ? item.o : item.c);
      h.push(item.h > 0 ? item.h : item.c);
      l.push(item.l > 0 ? item.l : item.c);
      c.push(item.c);
      v.push(item.vo);
    }
    s = StatusResponseEnum.OK.valueOf();
  }
  return {
    t: t,
    o: o,
    h: h,
    l: l,
    c: c,
    v: v,
    s: s,
    nextTime: null,
  };
};

const parseSymbolInfo = (symbolInfo: ISymbolInfo): any => {
  if (symbolInfo == null) {
    return { s: 'error', errmsg: 'unknown_symbol' };
  }
  return {
    name: symbolInfo.code,
    'exchange-traded': symbolInfo.marketType,
    'exchange-listed': symbolInfo.marketType,
    timezone: 'Asia/Bangkok',
    minmov: 1,
    minmov2: 0,
    pointvalue: 1,
    session: '0900-1500',
    has_intraday: true,
    intraday_multipliers: ['1'],
    has_no_volume: false,
    description: symbolInfo.name != null ? symbolInfo.name : symbolInfo.code,
    type: symbolInfo.type,
    pricescale: 100,
    ticker: symbolInfo.code,
    has_empty_bars: false,
  };
};

const toQuerySymbolSearchResponse = (symbolInfo: ISymbolInfo): TradingViewSymbolSearchResponse => {
  return {
    symbol: symbolInfo.code,
    full_name: symbolInfo.name,
    description: symbolInfo.name,
    exchange: symbolInfo.marketType,
    type: symbolInfo.type,
  };
};

const convertFromChart = (chart: IChart): any => {
  return {
    id: chart._id,
    name: chart.name,
    symbol: chart.symbol,
    resolution: chart.resolution,
    timestamp: Math.round(chart.lastModified.getTime() / 1000),
  };
};

const covertFromChartToChartLoadInfo = (chart: IChart): any => {
  return {
    id: chart._id,
    name: chart.name,
    content: chart.content,
    timestamp: Math.round(chart.lastModified.getTime() / 1000),
  };
};

const toStockRankingUpdownResponse = (symbolInfo: ISymbolInfo): any => {
  return {
    mt: symbolInfo.marketType,
    cd: symbolInfo.code,
    cl: symbolInfo.ceilingFloorEqual,
    d: Utils.formatDateToDisplay(symbolInfo.updatedAt, Utils.DATE_DISPLAY_FORMAT),
    o: symbolInfo.open,
    h: symbolInfo.high,
    l: symbolInfo.low,
    c: symbolInfo.last,
    ch: symbolInfo.change,
    r: symbolInfo.rate,
    tv: symbolInfo.tradingVolume,
    tr: symbolInfo.tradingValue,
    // "uc": symbolInfo.upDownChange,
    // "ur": symbolInfo.,
    // "sp": symbolInfo.startprice,
    // "ep": symbolInfo.
  };
};

const toStockRankingTopResponse = (
  symbolInfo: ISymbolInfo,
  sortType: string,
  upDownType: string
): StockRankingTopResponse => {
  let powerIndicator: number = 0;
  if (sortType === TopSortTypeEnum.POWER) {
    if (upDownType === UpDownTypeEnum.DOWN) {
      //DOWN mean sort POWER stock from best to worse, UP mean WEAK stock from worst to better
      powerIndicator = symbolInfo.bidPrice * symbolInfo.bidVolume;
    } else {
      powerIndicator = symbolInfo.offerPrice * symbolInfo.offerVolume;
    }
  }
  return {
    mt: symbolInfo.marketType,
    s: symbolInfo.code,
    cl: symbolInfo.ceilingFloorEqual,
    d: Utils.formatDateToDisplay(symbolInfo.updatedAt, Utils.DATE_DISPLAY_FORMAT),
    o: symbolInfo.open,
    h: symbolInfo.high,
    l: symbolInfo.low,
    c: symbolInfo.last,
    ch: symbolInfo.change,
    ra: symbolInfo.rate,
    vo: symbolInfo.tradingVolume,
    va: symbolInfo.tradingValue,
    pi: powerIndicator,
  };
};

const toSymbolTickSizeMatchResponse = (symbolQuoteList: ISymbolQuote[]): SymbolTickSizeMatchResponse => {
  const response: SymbolTickSizeMatchResponse = [];
  const priceList = [];
  const matchingVolumeList = [];
  if (symbolQuoteList.length > 0) {
    for (const symbolQuote of symbolQuoteList) {
      const last: number = symbolQuote.last;
      const matchingVolume: number = symbolQuote.matchingVolume;
      if (priceList.indexOf(last) === -1) {
        priceList.push(last);
        matchingVolumeList.push(matchingVolume);
      } else {
        const indexPrice = priceList.indexOf(last);
        matchingVolumeList[indexPrice] += matchingVolume;
      }
    }

    //sort priceList
    for (let i = 0; i < priceList.length - 1; i++) {
      for (let j = i + 1; j < priceList.length; j++) {
        if (priceList[i] > priceList[j]) {
          const temp_price = priceList[i];
          priceList[i] = priceList[j];
          priceList[j] = temp_price;
          const temp_matching = matchingVolumeList[i];
          matchingVolumeList[i] = matchingVolumeList[j];
          matchingVolumeList[j] = temp_matching;
        }
      }
    }

    for (let i = 0; i < priceList.length; i++) {
      response.push({
        p: priceList[i],
        mv: matchingVolumeList[i],
      });
    }
  }
  return response;
};

const toTopForeignerTradingResponse = (symbolInfo: ISymbolInfo): TopForeignerTradingResponse => {
  const response: TopForeignerTradingResponse = {};
  response.s = symbolInfo.code;
  response.o = Utils.round(symbolInfo.open);
  response.h = Utils.round(symbolInfo.high);
  response.l = Utils.round(symbolInfo.low);
  response.c = Utils.round(symbolInfo.last);
  response.ch = Utils.round(symbolInfo.change);
  response.ra = Utils.round(symbolInfo.rate);
  response.vo = Utils.round(symbolInfo.tradingVolume);
  response.mt = symbolInfo.marketType;
  response.fbv = Utils.round(symbolInfo.foreignerBuyVolume * symbolInfo.last);
  response.fsv = Utils.round(symbolInfo.foreignerSellVolume * symbolInfo.last);
  response.fnv = Utils.round(response.fbv - response.fsv);
  return response;
};

const toTopAiRatingResponse = (recordList: ITopAiRating[]): TopAiRatingResponse => {
  return recordList.map((item: ITopAiRating) => {
    return {
      code: item.code,
      date: item.date,
      techScore: item.techScore,
      valuationScore: item.valuationScore,
      gsScore: item.gsScore,
      overall: item.overall,
      price: item.price,
      change: item.change,
    };
  });
};

export {
  toTopForeignerTradingResponse,
  toSymbolLatestResponse,
  toSymbolDailyResponse,
  toSymbolWeeklyOrMonthlyResponse,
  toSymbolQuoteResponse,
  toMarketSessionStatusResponse,
  toSymbolQuoteMinutesResponse,
  toSymbolQuoteTickResponse,
  toEtfIndexDailyResponse,
  toEtfNavDailyResponse,
  toForeignerDailyResponse,
  toPutthroughAdvertiseResponse,
  toStockRankingTradeResponse,
  toPutthroughDealResponse,
  toSymbolStaticInfoResponse,
  toIndexStockListResponse,
  parseFromSymbolInfoToFixSymbol,
  parseSymbolQuoteMinuteList,
  parseTradingviewDailyPeriodList,
  parseSymbolInfo,
  toQuerySymbolSearchResponse,
  convertFromChart,
  covertFromChartToChartLoadInfo,
  toStockRankingUpdownResponse,
  toStockRankingTopResponse,
  toSymbolTickSizeMatchResponse,
  toTopAiRatingResponse,
};
