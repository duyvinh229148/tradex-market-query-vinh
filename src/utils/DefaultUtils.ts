import * as moment from 'moment';
import { Utils } from 'tradex-common';
import { MARKET_TIMEZONE } from '../constants';

export let defaultBaseTime = (format: string = Utils.TIME_DISPLAY_FORMAT): string => {
  return moment(Utils.getEndOfDate(new Date())).format(format);
};

export let defaultBaseDate = (): Date => {
  const baseDate: Date = new Date();
  baseDate.setHours(baseDate.getHours() + MARKET_TIMEZONE);
  baseDate.setDate(baseDate.getDate() + 1); // default baseDate = tomorrow
  return Utils.getStartOfDate(baseDate);
};
