import { Db, MongoClient } from 'mongodb';
import config from '../config';

let database: Db;

export function connectToMongo(): Promise<Db> {
  return new Promise((resolve: Function, reject: Function) => {
    MongoClient.connect(
      config.db.connection.url,
      { useNewUrlParser: true, useUnifiedTopology: true },
      (err: any, db: MongoClient) => {
        if (err != null) {
          reject(err);
        }
        database = db.db(config.db.connection.database);
        // Set log level
        // Logger.setLevel('info');
        resolve(database);
      }
    );
  });
}

export function getDb(): Db {
  return database;
}
