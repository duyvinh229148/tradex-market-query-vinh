import { Service } from 'typedi';
import { getDb } from '../utils/dbConnection';
import { Cursor, FilterQuery } from 'mongodb';
import { COLLECTIONS_NAME, DEFAULT_PAGE_SIZE } from '../constants';
import { ISymbolQuote } from '../models/db/ISymbolQuote';
import { Logger } from 'tradex-common';

@Service()
export class SymbolQuoteRepository {
  public findBy(query: FilterQuery<ISymbolQuote>, limit: number = DEFAULT_PAGE_SIZE, sort: any): Cursor<ISymbolQuote> {
    return getDb()
      .collection(COLLECTIONS_NAME.SYMBOL_QUOTE)
      .find(query)
      .sort(sort)
      .limit(limit);
  }

  public deleteAll(): Promise<any> {
    return new Promise((resolve: Function, reject: Function) => {
      getDb()
        .collection(COLLECTIONS_NAME.SYMBOL_QUOTE)
        .deleteMany({}, (err: any, result: any) => {
          if (err) {
            reject(err);
          } else {
            resolve(result);
          }
        });
    });
  }

  public moveToHistory() {
    const cached: any[] = [];
    const maxCacheSize = 100;
    const cursor: Cursor<ISymbolQuote> = getDb()
      .collection(COLLECTIONS_NAME.SYMBOL_QUOTE)
      .find({});

    cursor
      .forEach(async (value: any) => {
        cached.push(value);
        if (cached.length >= maxCacheSize) {
          const data = [...cached];
          cached.length = 0;
          getDb()
            .collection(COLLECTIONS_NAME.SYMBOL_QUOTE_HISTORY)
            .insertMany(data)
            .then()
            .catch();
        }
      })
      .then(() => {
        if (cached.length > 0) {
          getDb()
            .collection(COLLECTIONS_NAME.SYMBOL_QUOTE_HISTORY)
            .insertMany(cached)
            .then()
            .catch();
        }
        Logger.info('Store Symbol Quote To History Done');
      })
      .catch();
  }
}
