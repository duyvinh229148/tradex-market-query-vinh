import { Service } from 'typedi';
import { getDb } from '../utils/dbConnection';
import { Cursor, FilterQuery } from 'mongodb';
import { COLLECTIONS_NAME, DEFAULT_OFFSET, DEFAULT_PAGE_SIZE } from '../constants';
import { IDealNoticeData as DealNoticeData } from '../models/db/IDealNoticeData';

@Service()
export class DealNoticeDataRepository {
  public findBy(
    query: FilterQuery<DealNoticeData>,
    limit: number = DEFAULT_PAGE_SIZE,
    offset: number = DEFAULT_OFFSET,
    sort: any = {}
  ): Cursor<DealNoticeData> {
    return getDb()
      .collection(COLLECTIONS_NAME.DEAL_NOTICE_DATA)
      .find(query)
      .sort(sort)
      .skip(offset)
      .limit(limit);
  }

  public async deleteAll(): Promise<any> {
    return getDb()
      .collection(COLLECTIONS_NAME.DEAL_NOTICE_DATA)
      .deleteMany({});
  }
}
