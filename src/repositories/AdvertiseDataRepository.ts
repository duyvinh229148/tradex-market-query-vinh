import { Service } from 'typedi';
import { getDb } from '../utils/dbConnection';
import { Cursor, FilterQuery } from 'mongodb';
import { COLLECTIONS_NAME, DEFAULT_OFFSET, DEFAULT_PAGE_SIZE } from '../constants';
import { IAdvertiseData } from '../models/db/IAdvertiseData';

@Service()
export class AdvertiseDataRepository {
  public findBy(
    query: FilterQuery<IAdvertiseData>,
    limit: number = DEFAULT_PAGE_SIZE,
    offset: number = DEFAULT_OFFSET,
    sort: any = {}
  ): Cursor<IAdvertiseData> {
    return getDb()
      .collection(COLLECTIONS_NAME.ADVERTISE_DATA)
      .find(query)
      .sort(sort)
      .skip(offset)
      .limit(limit);
  }
}
