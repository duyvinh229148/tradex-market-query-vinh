import { Service } from 'typedi';
import { FilterQuery, Cursor, BulkWriteResult } from 'mongodb';
import { DEFAULT_PAGE_SIZE, COLLECTIONS_NAME } from '../constants';
import { getDb } from '../utils/dbConnection';
import { ISymbolQuoteMinutes } from '../models/db/ISymbolQuoteMinutes';

@Service()
export class SymbolQuoteMinutesRepository {
  public findBy(
    filter: FilterQuery<ISymbolQuoteMinutes>,
    limit: number = DEFAULT_PAGE_SIZE,
    sort: any = {}
  ): Cursor<ISymbolQuoteMinutes> {
    return getDb()
      .collection(COLLECTIONS_NAME.SYMBOL_QUOTE_MINUTE)
      .find(filter)
      .sort(sort)
      .limit(limit);
  }

  public async updateByBulk(listSymbolMinute: ISymbolQuoteMinutes[]): Promise<BulkWriteResult> {
    const bulk = getDb()
      .collection(COLLECTIONS_NAME.SYMBOL_QUOTE_MINUTE)
      .initializeOrderedBulkOp();
    for (let i = 0; i < listSymbolMinute.length; i++) {
      const symbolMinute = listSymbolMinute[i];
      bulk.find({ _id: symbolMinute._id }).updateOne(symbolMinute);
    }
    return bulk.execute();
  }
}
