import { Inject, Service } from 'typedi';
import RedisService, { REDIS_KEY } from '../RedisService';
import { ISymbolQuoteMinutes } from '../../models/db/ISymbolQuoteMinutes';
import { ISymbolInfo } from '../../models/db/ISymbolInfo';
import { Logger, Utils } from 'tradex-common';
import { FilterQuery } from 'mongodb';
import { PERIOD_TYPE, SecuritiesTypeEnum } from '../../constants';
import { getKeySymbolQuoteMinute, getMonthKey, getSixMonthKey, getWeekKey } from '../../utils/parse';
import { SymbolPeriodResponse } from 'tradex-models-market';
import { ISymbolDaily } from '../../models/db/ISymbolDaily';
import { toSymbolDailyResponse, toSymbolWeeklyOrMonthlyResponse } from '../../utils/ResponseUtils';
import { ISymbolWeeklyOrMonthly } from '../../models/db/ISymbolWeeklyOrMonthly';
import { SymbolQuoteMinutesRepository } from '../../repositories/SymbolQuoteMinutesRepository';
import { SymbolDailyRepository } from '../../repositories/SymbolDailyRepository';

@Service()
export default class CommonService {
  @Inject()
  private readonly redisService: RedisService;
  @Inject()
  private readonly symbolQuoteMinutesRepository: SymbolQuoteMinutesRepository;
  @Inject()
  private readonly symbolDailyRepository: SymbolDailyRepository;

  public async actualQueryQuoteMinuteThenGrouped(
    symbol: string,
    fromTime: Date,
    toTime: Date,
    fetchCount: number,
    minuteUnit: number
  ): Promise<ISymbolQuoteMinutes[]> {
    //get a bit more, group it, then take (fetchCount) first grouped records
    let limit: number = (fetchCount + 1) * minuteUnit;
    let redisSQMList: ISymbolQuoteMinutes[] = await this.redisService.lrange(
      `${REDIS_KEY.SYMBOL_QUOTE_MINUTE}_${symbol}`,
      0,
      -1
    );
    if (redisSQMList != null) {
      redisSQMList = redisSQMList.filter((item: ISymbolQuoteMinutes) => item.date <= toTime && item.date >= fromTime);
      redisSQMList = redisSQMList.splice(0, limit);
      limit = limit - redisSQMList.length;
    } else {
      redisSQMList = [];
    }

    //query mongo
    let dbSMQList: ISymbolQuoteMinutes[] = [];
    if (limit > 0) {
      const symbolInfo: ISymbolInfo = await this.redisService.getSymbolInfo(symbol);
      if (symbolInfo == null) {
        Logger.info(`No symbol info ${symbol} in redis, skip query db`);
      } else {
        //normally, mongodb won't have today records, but if manually dump or save by job, it will
        //so skip duplicate record
        let newToTime: Date = new Date(toTime);
        if (redisSQMList.length > 0) {
          //get the earliest time in redis to be newToTime
          const redisLastTime = redisSQMList[redisSQMList.length - 1].date;
          if (redisLastTime < toTime) {
            newToTime = redisSQMList[redisSQMList.length - 1].date;
          }
        }
        const query: FilterQuery<ISymbolQuoteMinutes> = {
          date: {
            $lte: newToTime,
            $gte: fromTime,
          },
        };
        if (symbolInfo.type === SecuritiesTypeEnum.FUTURES) {
          //redis save symbolInfo futures in code (like "VN30F1901"), need to query from db by refCode (like "VN30F1M")
          query.refCode = symbolInfo.refCode;
        } else {
          query.code = symbol;
        }
        dbSMQList = await this.symbolQuoteMinutesRepository.findBy(query, limit, { date: -1 }).toArray();
      }
    }
    const symbolQuoteMinutesList: ISymbolQuoteMinutes[] = redisSQMList.concat(dbSMQList);
    //group based on minute unit
    const minuteDict = {};
    for (const current of symbolQuoteMinutesList) {
      const key = getKeySymbolQuoteMinute(current, minuteUnit);
      if (minuteDict[key] == null) {
        minuteDict[key] = current;
      } else {
        const placeHolderRecord: ISymbolQuoteMinutes = minuteDict[key];
        placeHolderRecord.periodTradingVolume = placeHolderRecord.periodTradingVolume + current.periodTradingVolume;
        placeHolderRecord.high = Utils.round(Math.max(placeHolderRecord.high, current.high));
        placeHolderRecord.low = Utils.round(Math.min(placeHolderRecord.low, current.low));
        if (placeHolderRecord.milliseconds < current.milliseconds) {
          placeHolderRecord.last = Utils.round(current.last);
          placeHolderRecord.milliseconds = current.milliseconds;
          placeHolderRecord.tradingValue = Utils.round(current.tradingValue);
          placeHolderRecord.tradingVolume = Utils.round(current.tradingVolume);
        } else {
          placeHolderRecord.open = Utils.round(current.open);
        }
        minuteDict[key] = placeHolderRecord;
      }
    }
    return Object.values(minuteDict).splice(0, fetchCount);
  }

  public async actualQuerySymbolPeriod(
    symbol: string,
    periodType: string,
    fetchCount: number,
    baseDate: Date
  ): Promise<SymbolPeriodResponse[]> {
    //get a bit more, sort date: -1, group it, then take (fetchCount) first grouped records
    let dayUnit: number = 1;
    switch (periodType) {
      case PERIOD_TYPE.DAILY:
        dayUnit = 1;
        break;
      case PERIOD_TYPE.WEEKLY:
        dayUnit = 7;
        break;
      case PERIOD_TYPE.MONTHLY:
        dayUnit = 31;
        break;
      case PERIOD_TYPE.SIX_MONTH:
        dayUnit = 31 * 6;
        break;
      default:
        return [];
    }
    const limit = (fetchCount + 1) * dayUnit;

    let symbolDailyList: ISymbolDaily[] = await this.symbolDailyRepository
      .findBy(
        {
          code: symbol,
          date: { $lt: baseDate },
        },
        limit,
        0,
        {
          date: -1,
        }
      )
      .toArray();
    if (symbolDailyList.length === 0) {
      return [];
    }

    if (baseDate > Utils.getStartOfDate(new Date()) && symbolDailyList[0].date > Utils.getStartOfDate(new Date())) {
      const currentSymbolDaily: ISymbolDaily = await this.redisService.hget<ISymbolDaily>(
        REDIS_KEY.SYMBOL_DAILY,
        symbol
      );
      if (currentSymbolDaily != null) {
        symbolDailyList[0] = currentSymbolDaily;
      }
    }

    if (periodType === PERIOD_TYPE.DAILY) {
      symbolDailyList = symbolDailyList.splice(0, fetchCount);
      return symbolDailyList.map(toSymbolDailyResponse);
    }
    //calculate weekly or monthly
    const periodDict = {};
    for (const current of symbolDailyList) {
      let key: string;
      if (periodType === PERIOD_TYPE.WEEKLY) {
        key = getWeekKey(current);
      } else if (periodType === PERIOD_TYPE.MONTHLY) {
        key = getMonthKey(current);
      } else {
        key = getSixMonthKey(current);
      }

      if (periodDict[key] == null) {
        periodDict[key] = [];
      }
      periodDict[key].push(current);
    }
    let finalPeriodList: ISymbolWeeklyOrMonthly[] = [];
    for (const key of Object.keys(periodDict)) {
      const groupedList: ISymbolDaily[] = periodDict[key];
      //sort date: -1
      const sortedList: ISymbolDaily[] = groupedList.sort((a: ISymbolDaily, b: ISymbolDaily) => +b.date - +a.date);
      const calculatedRecord = this.manualCalculateWeeklyMonthly(sortedList);
      finalPeriodList.push(calculatedRecord);
    }
    finalPeriodList = finalPeriodList.splice(0, fetchCount);
    return finalPeriodList.map(toSymbolWeeklyOrMonthlyResponse);
  }

  public manualCalculateWeeklyMonthly(symbolDailyList: ISymbolDaily[]): ISymbolWeeklyOrMonthly {
    // symbolDailyList are sorting from latest to the past, like [Friday, Monday, Wed, Tuesday, Monday]
    const firstDay: ISymbolDaily = symbolDailyList[symbolDailyList.length - 1];
    const lastDay: ISymbolDaily = symbolDailyList[0];

    let high: number = symbolDailyList[0].high;
    let low: number = symbolDailyList[0].low;
    let totalTradingValue: number = 0;
    let totalTradingVolume: number = 0;
    let dayCount: number = 0;

    for (const symbolDaily of symbolDailyList) {
      dayCount++;
      if (high < symbolDaily.high) {
        high = symbolDaily.high;
      }
      if (low > symbolDaily.low) {
        low = symbolDaily.low;
      }
      totalTradingValue += symbolDaily.tradingValue;
      totalTradingVolume += symbolDaily.tradingVolume;
    }

    const lastOfPreviousPeriod: number = firstDay.last - firstDay.change;
    const change = lastDay.last - lastOfPreviousPeriod;
    const rate: number = (change / lastOfPreviousPeriod) * 100;

    return {
      _id: firstDay._id,
      code: firstDay.code,
      open: firstDay.open,
      high: high,
      low: low,
      last: lastDay.last,
      change: change,
      rate: rate,
      tradingVolume: totalTradingVolume,
      tradingValue: totalTradingValue,
      date: firstDay.date,
      refCode: firstDay.refCode,
      dayCount: dayCount,
    };
  }
}
