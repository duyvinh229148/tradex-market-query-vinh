import { Inject, Service } from 'typedi';
import * as Ajv from 'ajv';
import {
  SymbolLatestRequest,
  SymbolLatestResponse,
  SymbolPeriodRequest,
  SymbolPeriodResponse,
  SymbolQuoteRequest,
  SymbolQuoteResponse,
  SymbolQuoteMinuteRequest,
  SymbolQuoteMinuteResponse,
  SymbolQuoteTickRequest,
  SymbolQuoteTickResponse,
  ForeignerDailyRequest,
  ForeignerDailyResponse,
  StockRankingTradeRequest,
  StockRankingTradeResponse,
  StockRankingUpDownRequest,
  StockRankingUpDownResponse,
  SymbolStaticInfoRequest,
  SymbolStaticInfoResponse,
  IndexStockListRequest,
  IndexStockListResponse,
  StockRankingTopRequest,
  StockRankingTopResponse,
  TopForeignerTradingRequest,
  TopForeignerTradingResponse,
  SymbolDailyReturnsRequest,
  SymbolDailyReturnsResponse,
  SymbolDailyReturnsInitRequest,
  SymbolTickSizeMatchRequest,
  SymbolTickSizeMatchResponse,
} from 'tradex-models-market';
import {
  symbolLatestRequestValidator,
  symbolPeriodRequestValidator,
  symbolQuoteRequestValidator,
  symbolQuoteMinuteRequestValidator,
  symbolQuoteTickRequestValidator,
  foreignerDailyRequestValidator,
  stockRankingTradeRequestValidator,
  stockRankingUpDownRequestValidator,
  symbolStaticInfoRequestValidator,
  indexStockListRequestValidator,
  stockRankingTopRequestValidator,
  topForeignerTradingRequestValidator,
  symbolDailyReturnsRequestValidator,
  symbolTickSizeMatchRequestValidator,
  symbolDailyReturnsInitRequestValidator,
} from 'tradex-models-market-validator';
import { Errors, Logger, Utils } from 'tradex-common';
import {
  DEFAULT_PAGE_SIZE,
  INVALID_PARAMETER,
  DEFAULT_CEILING_SEQUENCE,
  MARKET_TIMEZONE,
  MarketTypeEnum,
  UpDownTypeEnum,
  StockRankingTradeSortTypeEnum,
  DEFAULT_OFFSET,
  SecuritiesTypeEnum,
  DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE,
  DIVIDEND_INFO,
  TopSortTypeEnum,
  POWER_STOCK_SORT_THRESHOLD,
  DEFAULT_TOP_FOREIGNER_TRADING,
  DEFAULT_QUERY_DAILY_RETURN_DAYS,
  MONGO_MAX_SAFE_ARRAY_SIZE,
  DEFAULT_FLOOR_DATE,
} from '../constants';
import RedisService, { REDIS_KEY } from './RedisService';
import { ISymbolInfo } from '../models/db/ISymbolInfo';
import {
  toSymbolLatestResponse,
  toSymbolQuoteResponse,
  toSymbolQuoteMinutesResponse,
  toSymbolQuoteTickResponse,
  toStockRankingTradeResponse,
  toForeignerDailyResponse,
  toSymbolStaticInfoResponse,
  toIndexStockListResponse,
  toStockRankingTopResponse,
  toStockRankingUpdownResponse,
  toTopForeignerTradingResponse,
  toSymbolTickSizeMatchResponse,
} from '../utils/ResponseUtils';
import { FilterQuery } from 'mongodb';
import { ISymbolDaily } from '../models/db/ISymbolDaily';
import { SymbolDailyRepository } from '../repositories/SymbolDailyRepository';
import { SymbolWeeklyRepository } from '../repositories/SymbolWeeklyRepository';
import { ISymbolQuote } from '../models/db/ISymbolQuote';
import { SymbolQuoteRepository } from '../repositories/SymbolQuoteRepository';
import { ISymbolQuoteMinutes } from '../models/db/ISymbolQuoteMinutes';
import { SymbolQuoteMinutesRepository } from '../repositories/SymbolQuoteMinutesRepository';
import { IForeignerDaily } from '../models/db/IForeignerDaily';
import { ForeignerDailyRepository } from '../repositories/ForeignerDailyRepository';
import { IndexStockListRepository } from '../repositories/IndexStockListRepository';
import { ISymbolWeeklyOrMonthly } from '../models/db/ISymbolWeeklyOrMonthly';
import { ISymbolMonthly } from '../models/db/ISymbolMonthly';
import { SymbolMonthlyRepository } from '../repositories/SymbolMonthlyRepository';
import { IDividendInfo } from '../models/db/IDividenInfo';
import { ISymbolQuoteMinutesBackup } from '../models/db/ISymbolQuoteMinutesBackup';
import { SymbolQuoteMinutesBackupRepository } from '../repositories/SymbolQuoteMinutesBackupRepository';
import { SymbolQuoteBackupRepository } from '../repositories/SymbolQuoteBackupRepository';
import { ISymbolQuoteBackup } from '../models/db/ISymbolQuoteBackup';
import { SymbolInfoRepository } from '../repositories/SymbolInfoRepository';
import { IGroupedSymbolDailyResponse } from '../models/response/IGroupedSymbolDailyResponse';
import { ISymbolPrevious } from '../models/db/ISymbolPrevious';
import { SymbolPreviousRepository } from '../repositories/SymbolPreviousRepository';
import { getKeySymbolQuoteTick, validateRequest } from '../utils/parse';
import CommonService from './common/CommonService';

@Service()
export default class SymbolService {
  @Inject()
  private readonly redisService: RedisService;
  @Inject()
  private readonly symbolDailyRepository: SymbolDailyRepository;
  @Inject()
  private readonly symbolPreviousRepository: SymbolPreviousRepository;
  @Inject()
  private readonly symbolWeeklyRepository: SymbolWeeklyRepository;
  @Inject()
  private readonly symbolQuoteRepository: SymbolQuoteRepository;
  @Inject()
  private readonly symbolQuoteMinutesRepository: SymbolQuoteMinutesRepository;
  @Inject()
  private readonly foreignerDailyRepository: ForeignerDailyRepository;
  @Inject()
  private readonly indexStockListRepository: IndexStockListRepository;
  @Inject()
  private readonly symbolMonthlyRepository: SymbolMonthlyRepository;
  @Inject()
  private readonly symbolQuoteMinutesBackupRepository: SymbolQuoteMinutesBackupRepository;
  @Inject()
  private readonly symbolQuoteBackupRepository: SymbolQuoteBackupRepository;
  @Inject()
  private readonly symbolInfoRepository: SymbolInfoRepository;
  @Inject()
  private readonly commonService: CommonService;

  public async queryIndexStockList(request: IndexStockListRequest): Promise<IndexStockListResponse> {
    const validator: Ajv.ValidateFunction = indexStockListRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const query: FilterQuery<ISymbolQuote> = {
      _id: request.indexCode,
    };
    const indexStockList = await this.indexStockListRepository.findBy(query).toArray();
    if (indexStockList == null || indexStockList.length === 0) {
      return [];
    }
    return toIndexStockListResponse(indexStockList[0]);
  }

  public async querySymbolQuote(request: SymbolQuoteRequest): Promise<SymbolQuoteResponse[]> {
    const validator: Ajv.ValidateFunction = symbolQuoteRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    let fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    let count: number = 0;

    const lastTradingVolume: number =
      request.lastTradingVolume != null ? request.lastTradingVolume : Number.MAX_SAFE_INTEGER;
    // query redis. Redis are sorted from latest to oldest
    const listSymbolQuote: ISymbolQuote[] = await this.redisService.lrange(
      `${REDIS_KEY.SYMBOL_QUOTE}_${request.symbol}`,
      0,
      -1
    );

    let response: ISymbolQuote[] = [];
    let tradingVolumeFilter: number = lastTradingVolume;
    if (listSymbolQuote.length > 0) {
      for (const symbolQuote of listSymbolQuote) {
        if (symbolQuote.tradingVolume < lastTradingVolume) {
          response.push(symbolQuote);
          count = count + 1;
        }
        if (count === fetchCount) {
          return response.map(toSymbolQuoteResponse);
        }
      }
      fetchCount = fetchCount - count;
      tradingVolumeFilter = listSymbolQuote[listSymbolQuote.length - 1].tradingVolume;
    }

    const today: Date = new Date();
    const query: FilterQuery<ISymbolQuote> = {
      code: request.symbol,
      tradingVolume: { $lt: tradingVolumeFilter },
      date: {
        $gte: Utils.getStartOfDate(today),
        $lte: Utils.getEndOfDate(today),
      },
    };

    const dbSymbolQuoteList: ISymbolQuote[] = await this.symbolQuoteRepository
      .findBy(query, fetchCount, { tradingVolume: -1 })
      .toArray();
    if (dbSymbolQuoteList.length > 0) {
      response = response.concat(dbSymbolQuoteList);
    }
    return response.map(toSymbolQuoteResponse);
  }

  public async querySymbolQuoteMinutes(request: SymbolQuoteMinuteRequest): Promise<SymbolQuoteMinuteResponse[]> {
    //query from redis, then from mongo, then group based on minute unit
    validateRequest(request, symbolQuoteMinuteRequestValidator);

    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const minuteUnit: number = request.minuteUnit;
    const today = new Date();
    const fromTime =
      request.fromTime != null
        ? Utils.convertStringToDate(request.fromTime, Utils.DATETIME_DISPLAY_FORMAT)
        : DEFAULT_FLOOR_DATE;
    const toTime =
      request.toTime != null ? Utils.convertStringToDate(request.toTime, Utils.DATETIME_DISPLAY_FORMAT) : today;
    const responseList: ISymbolQuoteMinutes[] = await this.commonService.actualQueryQuoteMinuteThenGrouped(
      request.symbol,
      fromTime,
      toTime,
      fetchCount,
      minuteUnit
    );
    return responseList.map((item: ISymbolQuoteMinutes) => toSymbolQuoteMinutesResponse(item, minuteUnit));
  }

  public async querySymbolQuoteTick(request: SymbolQuoteTickRequest): Promise<SymbolQuoteTickResponse[]> {
    validateRequest(request, symbolQuoteTickRequestValidator);
    //query from redis, then mongo, group, then calculate periodTradingVolume
    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const toSequence: number = request.toSequence != null ? request.toSequence : DEFAULT_CEILING_SEQUENCE;
    const fromSequence: number = request.fromSequence != null ? request.fromSequence : 0;
    const tickUnit: number = request.tickUnit;

    //get a bit more, group it, then take (fetchCount) first grouped records
    //get (fetchCount+2) because need to get 1 more record than normal (after grouped) to calculate periodTradingVolume
    let limit: number = (fetchCount + 2) * tickUnit;
    let redisSymbolQuoteTickList: ISymbolQuote[] = await this.redisService.lrange(
      `${REDIS_KEY.SYMBOL_QUOTE}_${request.symbol}`,
      0,
      -1
    );
    redisSymbolQuoteTickList = redisSymbolQuoteTickList.filter(
      (item: ISymbolQuote) => item.sequence < toSequence && item.sequence > fromSequence
    );
    //sort from higher sequence to lower sequence
    redisSymbolQuoteTickList = redisSymbolQuoteTickList.sort((a: ISymbolQuote, b: ISymbolQuote) => {
      return -(a.sequence - b.sequence);
    });
    redisSymbolQuoteTickList = redisSymbolQuoteTickList.splice(0, limit);
    limit = limit - redisSymbolQuoteTickList.length;

    //query mongo
    let dbSMTList: ISymbolQuoteMinutes[] = [];
    if (limit > 0) {
      //normally, mongodb won't have today records, but if manually dump or save by job, it will
      //so skip duplicate record
      let newToSequence: number = toSequence;
      if (redisSymbolQuoteTickList.length > 0) {
        //get the earliest sequence in redis to be newToSequence
        const redisEarliestSequence = redisSymbolQuoteTickList[redisSymbolQuoteTickList.length - 1].sequence;
        if (redisEarliestSequence < toSequence) {
          newToSequence = redisSymbolQuoteTickList[redisSymbolQuoteTickList.length - 1].sequence;
        }
      }

      const query: FilterQuery<ISymbolQuote> = {
        code: request.symbol,
        sequence: {
          $lt: newToSequence,
          $gt: fromSequence,
        },
        date: {
          $gte: Utils.getStartOfDate(new Date()),
          $lte: Utils.getEndOfDate(new Date()),
        },
      };
      dbSMTList = await this.symbolQuoteRepository.findBy(query, limit, { sequence: -1 }).toArray();
    }
    const totalSymbolQuoteTickList: ISymbolQuote[] = redisSymbolQuoteTickList.concat(dbSMTList);

    const quoteTickDict = {};
    for (const current of totalSymbolQuoteTickList) {
      const key = getKeySymbolQuoteTick(current, tickUnit);
      if (quoteTickDict[key] == null) {
        quoteTickDict[key] = current;
      } else {
        const placeHolderRecord: ISymbolQuote = quoteTickDict[key];

        placeHolderRecord.high = Utils.round(Math.max(placeHolderRecord.high, current.high));
        placeHolderRecord.low = Utils.round(Math.min(placeHolderRecord.low, current.low));
        if (placeHolderRecord.sequence < current.sequence) {
          //current record have higher sequence, come later
          placeHolderRecord.tradingVolume = current.tradingVolume;
          placeHolderRecord.last = Utils.round(current.last);
          placeHolderRecord.date = current.date;
        } else {
          placeHolderRecord.open = Utils.round(current.open);
        }
        quoteTickDict[key] = placeHolderRecord;
      }
    }
    //get 1 more record to calculate periodTradingVolume
    const groupedList: ISymbolQuote[] = Object.values(quoteTickDict).splice(0, fetchCount + 1);
    let higherSequenceRecord: ISymbolQuote = null;

    const responseList: SymbolQuoteTickResponse[] = [];
    for (const current of groupedList) {
      let periodTradingVolume: number = 0;
      if (higherSequenceRecord != null) {
        //in first loop, higherSequenceRecord = current, skip this block
        periodTradingVolume = higherSequenceRecord.tradingVolume - current.tradingVolume;
        //push the previous record, which have periodTradingVolume calculated
        responseList.push(toSymbolQuoteTickResponse(higherSequenceRecord, periodTradingVolume)); //responseList will only push (fetchCount) time, not (fetchCount+1)
      }

      higherSequenceRecord = current;
    }
    return responseList;
  }

  public async querySymbolLatest(request: SymbolLatestRequest): Promise<SymbolLatestResponse[]> {
    const validator: Ajv.ValidateFunction = symbolLatestRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const response: SymbolLatestResponse[] = [];
    if (request.symbolList == null || request.symbolList.length === 0) {
      return [];
    }

    for (let i = 0; i < request.symbolList.length; i++) {
      const symbolInfo: ISymbolInfo = await this.redisService.hget<ISymbolInfo>(
        REDIS_KEY.SYMBOL_INFO,
        request.symbolList[i]
      );
      if (symbolInfo != null) {
        response.push(toSymbolLatestResponse(symbolInfo));
      }
    }
    return response;
  }

  public async querySymbolStaticInfo(request: SymbolStaticInfoRequest): Promise<SymbolStaticInfoResponse[]> {
    const validator: Ajv.ValidateFunction = symbolStaticInfoRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const response: SymbolStaticInfoResponse[] = [];
    if (request.symbolList == null || request.symbolList.length === 0) {
      const allSymbolInfo: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);
      return allSymbolInfo.map(toSymbolStaticInfoResponse);
    }

    for (let i = 0; i < request.symbolList.length; i++) {
      const symbolInfo: ISymbolInfo = await this.redisService.hget<ISymbolInfo>(
        REDIS_KEY.SYMBOL_INFO,
        request.symbolList[i]
      );
      if (symbolInfo != null) {
        response.push(toSymbolStaticInfoResponse(symbolInfo));
      }
    }
    return response;
  }

  public async querySymbolPeriod(request: SymbolPeriodRequest): Promise<SymbolPeriodResponse[]> {
    validateRequest(request, symbolPeriodRequestValidator);

    //query from mongo, update today from redis, group by period
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);
    const baseDate: Date = request.baseDate != null ? Utils.convertStringToDate(request.baseDate) : tomorrow;
    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    return this.commonService.actualQuerySymbolPeriod(request.symbol, request.periodType, fetchCount, baseDate);
  }

  public async querySymbolForeignerDaily(request: ForeignerDailyRequest): Promise<ForeignerDailyResponse> {
    const validator: Ajv.ValidateFunction = foreignerDailyRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const currentDate = new Date();
    currentDate.setHours(currentDate.getHours() + MARKET_TIMEZONE);
    currentDate.setDate(currentDate.getDate() + 1);
    let fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const baseDate: Date = request.baseDate != null ? Utils.convertStringToDate(request.baseDate) : currentDate;
    let symbolForeignerDailyList: IForeignerDaily[] = [];
    const symbolForeignerDaily: IForeignerDaily = await this.redisService.hget(
      REDIS_KEY.FOREIGNER_DAILY,
      request.symbol
    );
    if (symbolForeignerDaily != null) {
      symbolForeignerDailyList.push(symbolForeignerDaily);
      fetchCount = fetchCount - 1;
    }
    if (fetchCount > 0) {
      const query: FilterQuery<IForeignerDaily> = {
        code: request.symbol,
        date: { $lt: baseDate },
      };
      const symbolForeignerDaily = await this.foreignerDailyRepository
        .findBy(query, fetchCount, { date: -1 })
        .toArray();
      symbolForeignerDailyList = symbolForeignerDailyList.concat(symbolForeignerDaily);
    }
    const response: ForeignerDailyResponse[] = [];
    for (let i = 0; i < symbolForeignerDailyList.length; i++) {
      const item: IForeignerDaily = symbolForeignerDailyList[i];
      response.push(toForeignerDailyResponse(item));
    }
    return response;
  }

  public async queryStockRankingUpDown(request: StockRankingUpDownRequest): Promise<StockRankingUpDownResponse> {
    const validator: Ajv.ValidateFunction = stockRankingUpDownRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const offset: number = request.offset != null ? request.offset : DEFAULT_OFFSET;
    const marketType: string = request.marketType != null ? request.marketType : MarketTypeEnum.ALL;
    const upDownType: string = request.upDownType != null ? request.upDownType : UpDownTypeEnum.DOWN;

    const listSymbolInfo: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);

    const listSymbolInfoRanking: ISymbolInfo[] = listSymbolInfo.filter((item: ISymbolInfo) => {
      return item.type === SecuritiesTypeEnum.STOCK;
    });
    let response: StockRankingUpDownResponse;

    if (marketType === MarketTypeEnum.ALL) {
      const hnxList: ISymbolInfo[] = this.filterRankingUpDown(
        listSymbolInfoRanking,
        MarketTypeEnum.HNX,
        upDownType,
        fetchCount,
        offset
      );
      const hoseList: ISymbolInfo[] = this.filterRankingUpDown(
        listSymbolInfoRanking,
        MarketTypeEnum.HOSE,
        upDownType,
        fetchCount,
        offset
      );
      const upcomList: ISymbolInfo[] = this.filterRankingUpDown(
        listSymbolInfoRanking,
        MarketTypeEnum.UPCOM,
        upDownType,
        fetchCount,
        offset
      );

      response = {
        HNX: hnxList.map(toStockRankingUpdownResponse),
        HOSE: hoseList.map(toStockRankingUpdownResponse),
        UPCOM: upcomList.map(toStockRankingUpdownResponse),
      };
    } else {
      const result: ISymbolInfo[] = this.filterRankingUpDown(
        listSymbolInfo,
        marketType,
        upDownType,
        fetchCount,
        offset
      );
      response = {
        [marketType]: result.map(toStockRankingUpdownResponse),
      };
    }

    return response;
  }

  public filterRankingUpDown(
    listSymbolInfo: ISymbolInfo[],
    marketType: string,
    upDownType: string,
    fetchCount: number,
    offset: number
  ): ISymbolInfo[] {
    const fitlerMarketType: ISymbolInfo[] = listSymbolInfo.filter((item: ISymbolInfo) => {
      return item.marketType === marketType;
    });

    const sortUpDown: ISymbolInfo[] = fitlerMarketType.sort((a: ISymbolInfo, b: ISymbolInfo) => {
      const x = a.rate;
      const y = b.rate;
      if (upDownType === UpDownTypeEnum.UP) {
        return x - y;
      } else {
        return y - x;
      }
    });
    // off set and fetch count
    return sortUpDown.slice(offset, fetchCount);
  }

  public async querySymbolTickSizeMatch(request: SymbolTickSizeMatchRequest): Promise<SymbolTickSizeMatchResponse> {
    const validator: Ajv.ValidateFunction = symbolTickSizeMatchRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const symbol: string = request.symbol;
    // query redis. Redis are sorted from latest to oldest
    const listSymbolQuote: ISymbolQuote[] = await this.redisService.lrange(
      `${REDIS_KEY.SYMBOL_QUOTE}_${symbol}`,
      0,
      -1
    );
    return toSymbolTickSizeMatchResponse(listSymbolQuote);
  }

  public async queryStockRankingTop(request: StockRankingTopRequest): Promise<StockRankingTopResponse[]> {
    const validator: Ajv.ValidateFunction = stockRankingTopRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const offset: number = request.offset != null ? request.offset : DEFAULT_OFFSET;
    const marketType: string = request.marketType != null ? request.marketType : MarketTypeEnum.ALL;
    const sortType: string = request.sortType != null ? request.sortType : TopSortTypeEnum.TRADING_VOLUME;
    const upDownType: string = request.upDownType != null ? request.upDownType : UpDownTypeEnum.DOWN;

    const listSymbolInfo: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);
    let stockInfoList: ISymbolInfo[] = listSymbolInfo.filter((item: ISymbolInfo) => {
      return item.type === SecuritiesTypeEnum.STOCK;
    });
    // filter by marketType
    if (marketType !== MarketTypeEnum.ALL) {
      stockInfoList = stockInfoList.filter((item: ISymbolInfo) => {
        return item.marketType === marketType;
      });
    }

    // sort by sort type
    if (sortType !== TopSortTypeEnum.POWER) {
      stockInfoList = stockInfoList.sort((a: ISymbolInfo, b: ISymbolInfo) => {
        let result: number = 0;
        if (sortType === TopSortTypeEnum.TRADING_VOLUME) {
          result = a.tradingVolume - b.tradingVolume;
        } else if (sortType === TopSortTypeEnum.TRADING_VALUE) {
          result = a.tradingValue - b.tradingValue;
        } else if (sortType === TopSortTypeEnum.CHANGE) {
          result = a.change - b.change;
        } else if (sortType === TopSortTypeEnum.RATE) {
          result = a.rate - b.rate;
        }
        return upDownType === UpDownTypeEnum.UP ? result : -result;
      });
    } else {
      if (upDownType === UpDownTypeEnum.DOWN) {
        // DOWN mean sort POWER stock from best to worse, UP mean WEAK stock from worst to better
        // find power stock
        stockInfoList = stockInfoList.filter((stock: ISymbolInfo) => {
          return stock.bidVolume * stock.bidPrice > POWER_STOCK_SORT_THRESHOLD && stock.bidPrice === stock.ceilingPrice;
        });

        // sort by bidVolume * bidPrice
        stockInfoList = stockInfoList.sort((a: ISymbolInfo, b: ISymbolInfo) => {
          const x = a.bidVolume * a.bidPrice;
          const y = b.bidVolume * b.bidPrice;
          return -(x - y);
        });
      } else {
        // find weakest stocks
        stockInfoList = stockInfoList.filter((stock: ISymbolInfo) => {
          return (
            stock.offerVolume * stock.offerPrice > POWER_STOCK_SORT_THRESHOLD && stock.offerPrice === stock.floorPrice
          );
        });

        // sort by offerVolume * offerPrice
        stockInfoList = stockInfoList.sort((a: ISymbolInfo, b: ISymbolInfo) => {
          const x = a.offerVolume * a.offerPrice;
          const y = b.offerVolume * b.offerPrice;
          // sort by most selled stock to less
          return -(x - y);
        });
      }
    }

    // offset and fetch count
    const response: ISymbolInfo[] = stockInfoList.splice(offset, fetchCount);
    return response.map((item: ISymbolInfo) => {
      return toStockRankingTopResponse(item, sortType, upDownType);
    });
  }

  public async queryTopForeignerTrading(request: TopForeignerTradingRequest): Promise<TopForeignerTradingResponse[]> {
    const validator: Ajv.ValidateFunction = topForeignerTradingRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_TOP_FOREIGNER_TRADING;
    const offset: number = request.offset != null ? request.offset : DEFAULT_OFFSET;
    const marketType: string = request.marketType != null ? request.marketType : MarketTypeEnum.ALL;
    const upDownType: string = request.upDownType != null ? request.upDownType : UpDownTypeEnum.DOWN;

    const listSymbolInfo: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);
    let stockInfoList: ISymbolInfo[] = listSymbolInfo.filter((item: ISymbolInfo) => {
      return item.type === SecuritiesTypeEnum.STOCK;
    });

    // filter by marketType
    if (marketType !== MarketTypeEnum.ALL) {
      stockInfoList = stockInfoList.filter((item: ISymbolInfo) => {
        return item.marketType === marketType;
      });
    }

    const intSort = upDownType === UpDownTypeEnum.DOWN.valueOf() ? -1 : 1;

    // sort by foreignerBuyVolume * last - foreignerSellVolume * last
    stockInfoList = stockInfoList.sort((a: ISymbolInfo, b: ISymbolInfo) => {
      const x = a.foreignerBuyVolume * a.last - a.foreignerSellVolume * a.last;
      const y = b.foreignerBuyVolume * b.last - b.foreignerSellVolume * b.last;
      return intSort * (x - y);
    });

    // offset and fetch count
    const response: ISymbolInfo[] = stockInfoList.splice(offset, fetchCount);

    return response.map(toTopForeignerTradingResponse);
  }

  public async querySymbolRankingTrade(request: StockRankingTradeRequest): Promise<StockRankingTradeResponse> {
    const validator: Ajv.ValidateFunction = stockRankingTradeRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const offset: number = request.offset != null ? request.offset : DEFAULT_OFFSET;
    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_PAGE_SIZE;
    const marketType: string = request.marketType != null ? request.marketType : MarketTypeEnum.ALL;
    let listSymbolRankingTrade: ISymbolInfo[] = [];
    const listSymbolInfo: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);
    if (listSymbolInfo != null) {
      listSymbolRankingTrade = listSymbolInfo.filter((item: ISymbolInfo) => {
        return item.type === SecuritiesTypeEnum.STOCK;
      });
      if (marketType !== MarketTypeEnum.ALL.valueOf()) {
        listSymbolRankingTrade = listSymbolRankingTrade.filter((item: ISymbolInfo) => {
          return item.marketType === marketType;
        });
      }
      if (request.sortType === StockRankingTradeSortTypeEnum.TRADING_VOLUME) {
        listSymbolRankingTrade.sort((a: ISymbolInfo, b: ISymbolInfo) => {
          return b.tradingVolume - a.tradingVolume;
        });
      } else if (request.sortType === StockRankingTradeSortTypeEnum.TRADING_VALUE) {
        listSymbolRankingTrade.sort((a: ISymbolInfo, b: ISymbolInfo) => {
          return b.tradingValue - a.tradingValue;
        });
      } else {
        listSymbolRankingTrade.sort((a: ISymbolInfo, b: ISymbolInfo) => {
          return b.turnoverRate - a.turnoverRate;
        });
      }
    }
    const response: StockRankingTradeResponse[] = [];
    for (let i = offset; i < listSymbolRankingTrade.length; i++) {
      if (i >= fetchCount + offset) {
        break;
      }
      const item: ISymbolInfo = listSymbolRankingTrade[i];
      response.push(toStockRankingTradeResponse(item));
    }
    return response;
  }

  public async updateDailyStockByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    Logger.info(`__update daily: ${dividendInfo.code} _ ${fromDate}`);
    const query: FilterQuery<ISymbolDaily> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const symbolDailyList: ISymbolDaily[] = await this.symbolDailyRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, 0, sort)
      .toArray();
    Logger.info(
      `finish update daily adjusted price for ${JSON.stringify(dividendInfo)} _  fromDate: ${fromDate}, length: ${
        symbolDailyList.length
      }`
    );
    if (symbolDailyList.length > 0) {
      for (let i = 0; i < symbolDailyList.length; i++) {
        const symbolDaily: ISymbolDaily = symbolDailyList[i];
        this.updateAdjustedPriceForStockPeriod(symbolDaily, dividendInfo);
      }
      await this.symbolDailyRepository.updateByBulk(symbolDailyList);
      return this.updateDailyStockByDividend(dividendInfo, symbolDailyList[symbolDailyList.length - 1].date);
    }
  }

  public async updateWeeklyStockByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    Logger.info(`__update weekly: ${dividendInfo.code} _ ${fromDate}`);
    const query: FilterQuery<ISymbolWeeklyOrMonthly> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const stockWeeklyList: ISymbolWeeklyOrMonthly[] = await this.symbolWeeklyRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, 0, sort)
      .toArray();
    Logger.info(
      `finish update weekly adjusted price for ${JSON.stringify(dividendInfo)} _  fromDate: ${fromDate}, length: ${
        stockWeeklyList.length
      }`
    );
    if (stockWeeklyList.length > 0) {
      for (let i = 0; i < stockWeeklyList.length; i++) {
        const stockWeekly: ISymbolWeeklyOrMonthly = stockWeeklyList[i];
        this.updateAdjustedPriceForStockPeriod(stockWeekly, dividendInfo);
      }
      await this.symbolWeeklyRepository.updateByBulk(stockWeeklyList);
      return this.updateWeeklyStockByDividend(dividendInfo, stockWeeklyList[stockWeeklyList.length - 1].date);
    }
  }

  public async updateMonthlyStockByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    Logger.info(`__update monthly: ${dividendInfo.code} _ ${fromDate}`);
    const query: FilterQuery<ISymbolMonthly> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const stockMonthlyList: ISymbolMonthly[] = await this.symbolMonthlyRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, 0, sort)
      .toArray();
    Logger.info(
      `finish update monthly adjusted price for ${JSON.stringify(dividendInfo)} _ fromDate: ${fromDate}, length: ${
        stockMonthlyList.length
      }`
    );
    if (stockMonthlyList.length > 0) {
      for (let i = 0; i < stockMonthlyList.length; i++) {
        const stockMonthly: ISymbolMonthly = stockMonthlyList[i];
        this.updateAdjustedPriceForStockPeriod(stockMonthly, dividendInfo);
      }
      await this.symbolMonthlyRepository.updateByBulk(stockMonthlyList);
      return this.updateMonthlyStockByDividend(dividendInfo, stockMonthlyList[stockMonthlyList.length - 1].date);
    }
  }

  public async updateMinuteStockQuoteByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    const query: FilterQuery<ISymbolQuoteMinutes> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const stockMinuteList: ISymbolQuoteMinutes[] = await this.symbolQuoteMinutesRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, sort)
      .toArray();

    Logger.info(
      `finish update minuteQuote adjusted price for ${JSON.stringify(dividendInfo)} _ fromDate: ${fromDate}, length: ${
        stockMinuteList.length
      }`
    );
    if (stockMinuteList.length > 0) {
      for (const stockQuoteMinute of stockMinuteList) {
        this.updateAdjustedPriceForStockQuoteMinute(stockQuoteMinute, dividendInfo);
      }
      await this.symbolQuoteMinutesRepository.updateByBulk(stockMinuteList);
      return this.updateMinuteStockQuoteByDividend(dividendInfo, stockMinuteList[stockMinuteList.length - 1].date);
    }
  }

  public async updateSymbolMinuteQuoteBackupByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    const query: FilterQuery<ISymbolQuoteMinutesBackup> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const stockMinuteList: ISymbolQuoteMinutesBackup[] = await this.symbolQuoteMinutesBackupRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, sort)
      .toArray();

    Logger.info(
      `finish update backup minuteQuote adjusted price for ${JSON.stringify(
        dividendInfo
      )} _ fromDate: ${fromDate}, length: ${stockMinuteList.length}`
    );
    if (stockMinuteList.length > 0) {
      for (const stockQuoteMinute of stockMinuteList) {
        this.updateAdjustedPriceForStockQuoteMinuteBackup(stockQuoteMinute, dividendInfo);
      }
      await this.symbolQuoteMinutesBackupRepository.updateByBulk(stockMinuteList);
      return this.updateSymbolMinuteQuoteBackupByDividend(
        dividendInfo,
        stockMinuteList[stockMinuteList.length - 1].date
      );
    }
  }

  public async updateSymbolQuoteBackupByDividend(dividendInfo: IDividendInfo, fromDate: Date): Promise<void> {
    const query: FilterQuery<ISymbolQuoteBackup> = {
      code: dividendInfo.code,
    };
    const sort: any = {
      date: -1,
    };
    if (dividendInfo.lastExDividendDate != null) {
      query.date = { $lt: fromDate, $gte: dividendInfo.lastExDividendDate };
    } else {
      query.date = { $lt: fromDate };
    }
    const symbolQuoteList: ISymbolQuoteBackup[] = await this.symbolQuoteBackupRepository
      .findBy(query, DEFAULT_BATCH_PROCESS_ADJUSTED_PRICE, sort)
      .toArray();

    Logger.info(
      `finish update back up Quote adjusted price for ${JSON.stringify(
        dividendInfo
      )} _ fromDate: ${fromDate}, length: ${symbolQuoteList.length}`
    );
    if (symbolQuoteList.length > 0) {
      for (const stockQuoteMinute of symbolQuoteList) {
        this.updateAdjustedPriceForSymbolQuoteBackup(stockQuoteMinute, dividendInfo);
      }
      await this.symbolQuoteBackupRepository.updateByBulk(symbolQuoteList);
      return this.updateSymbolQuoteBackupByDividend(dividendInfo, symbolQuoteList[symbolQuoteList.length - 1].date);
    }
  }

  public updateAdjustedPriceForStockPeriod(dailyStock: ISymbolDaily, dividendInfo: IDividendInfo): void {
    let referencePrice = dailyStock.last - dailyStock.change;
    referencePrice = this.calculateAdjustedPrice(referencePrice, dividendInfo);
    dailyStock.high = this.calculateAdjustedPrice(dailyStock.high, dividendInfo);
    dailyStock.low = this.calculateAdjustedPrice(dailyStock.low, dividendInfo);
    dailyStock.last = this.calculateAdjustedPrice(dailyStock.last, dividendInfo);
    dailyStock.open = this.calculateAdjustedPrice(dailyStock.open, dividendInfo);
    dailyStock.change = dailyStock.last - referencePrice;
    dailyStock.rate = (dailyStock.change / referencePrice) * 100;
  }

  public async updateAdjustedPriceForSymbolPrevious(dividendInfo: IDividendInfo): Promise<void> {
    const symbolPrevious: ISymbolPrevious = await this.symbolPreviousRepository.findOneBy({ _id: dividendInfo.code });
    if (symbolPrevious == null) {
      Logger.error(`not found symbolPrevious: ${dividendInfo.code}`);
      return;
    }
    symbolPrevious.close = this.calculateAdjustedPrice(symbolPrevious.close, dividendInfo);
    symbolPrevious.previousClose = this.calculateAdjustedPrice(symbolPrevious.previousClose, dividendInfo);
    symbolPrevious.note = 'update adjusted price';
    symbolPrevious.updatedAt = new Date();
    await this.symbolPreviousRepository.updateByBulk([symbolPrevious]);
  }

  public updateAdjustedPriceForStockQuoteMinute(
    stockQuoteMinute: ISymbolQuoteMinutes,
    dividendInfo: IDividendInfo
  ): void {
    stockQuoteMinute.high = this.calculateAdjustedPrice(stockQuoteMinute.high, dividendInfo);
    stockQuoteMinute.low = this.calculateAdjustedPrice(stockQuoteMinute.low, dividendInfo);
    stockQuoteMinute.last = this.calculateAdjustedPrice(stockQuoteMinute.last, dividendInfo);
    stockQuoteMinute.open = this.calculateAdjustedPrice(stockQuoteMinute.open, dividendInfo);
  }

  public updateAdjustedPriceForStockQuoteMinuteBackup(
    stockQuoteMinute: ISymbolQuoteMinutesBackup,
    dividendInfo: IDividendInfo
  ): void {
    stockQuoteMinute.high = this.calculateAdjustedPrice(stockQuoteMinute.high, dividendInfo);
    stockQuoteMinute.low = this.calculateAdjustedPrice(stockQuoteMinute.low, dividendInfo);
    stockQuoteMinute.last = this.calculateAdjustedPrice(stockQuoteMinute.last, dividendInfo);
    stockQuoteMinute.open = this.calculateAdjustedPrice(stockQuoteMinute.open, dividendInfo);
  }

  public updateAdjustedPriceForSymbolQuoteBackup(symbolQuote: ISymbolQuoteBackup, dividendInfo: IDividendInfo): void {
    symbolQuote.high = this.calculateAdjustedPrice(symbolQuote.high, dividendInfo);
    symbolQuote.low = this.calculateAdjustedPrice(symbolQuote.low, dividendInfo);
    symbolQuote.last = this.calculateAdjustedPrice(symbolQuote.last, dividendInfo);
    symbolQuote.open = this.calculateAdjustedPrice(symbolQuote.open, dividendInfo);
  }

  public calculateAdjustedPrice = (price: number, dividendInfo: IDividendInfo) => {
    if (price == null || price <= 0) {
      Logger.error(`Invalid Price ${price}`);
      return 0;
    }
    const market =
      price +
      (dividendInfo.ratioIssue != null ? dividendInfo.ratioIssue : 0) *
        (dividendInfo.priceIssue != null ? dividendInfo.priceIssue : 0) -
      (dividendInfo.ratioDividend != null ? dividendInfo.ratioDividend * DIVIDEND_INFO.PAR_VALUE_VN : 0);
    const numberOfStock =
      (dividendInfo.ratioIssue != null ? dividendInfo.ratioIssue : 0) +
      (dividendInfo.ratioBonusShare != null ? dividendInfo.ratioBonusShare : 0) +
      1;
    return market / numberOfStock;
  };

  public async initSymbolDailyReturns(request?: SymbolDailyReturnsInitRequest): Promise<void> {
    const validator: Ajv.ValidateFunction = symbolDailyReturnsInitRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    //if don't have request input, calculate whole db
    const floorDate: Date =
      request == null || request.floorDate == null ? DEFAULT_FLOOR_DATE : Utils.convertStringToDate(request.floorDate);

    let allUnderlyingSymbolList: string[] = [];
    if (request == null || request.symbolList == null || request.symbolList.length === 0) {
      // find all underlying symbol list
      allUnderlyingSymbolList = await this.getAllUnderlyingSymbolList();
    } else {
      allUnderlyingSymbolList = request.symbolList;
    }

    //get all symbol daily, from floorDate to today
    const groupedSymbolDailyList: IGroupedSymbolDailyResponse[] = await this.symbolDailyRepository
      .queryGroupedSymbolDailyList(allUnderlyingSymbolList, MONGO_MAX_SAFE_ARRAY_SIZE, floorDate)
      .toArray();

    const finalToSavedList: ISymbolDaily[] = [];

    for (const singleCodeData of groupedSymbolDailyList) {
      // list of records of 1 single symbol code
      const itemList: ISymbolDaily[] = singleCodeData.items;

      let previousLastPrice: number = 0;
      // itemList have been sorted with date: -1, so need manual forloop
      for (let i = itemList.length; i > 0; i--) {
        const data: ISymbolDaily = itemList[i - 1];
        if (previousLastPrice === 0) {
          previousLastPrice = data.last;
          continue;
        }
        data.returns = Math.log(data.last / previousLastPrice);
        finalToSavedList.push(data);
        previousLastPrice = data.last;
      }
    }
    Logger.info(
      `============Done init returns for ${finalToSavedList.length} records of ${allUnderlyingSymbolList.length} symbol========`
    );
    await this.symbolDailyRepository.updateReturnsByBulk(finalToSavedList);
  }

  public async getAllUnderlyingSymbolList(): Promise<string[]> {
    const allCwList: ISymbolInfo[] = await this.symbolInfoRepository
      .findBy(
        {
          type: SecuritiesTypeEnum.CW,
        },
        Number.MAX_SAFE_INTEGER
      )
      .toArray();

    const cwSet = new Set<string>();
    for (const cw of allCwList) {
      cwSet.add(cw.underlyingSymbol.trim());
    }
    return [...cwSet];
  }

  public async querySymbolDailyReturns(request: SymbolDailyReturnsRequest): Promise<SymbolDailyReturnsResponse> {
    const validator: Ajv.ValidateFunction = symbolDailyReturnsRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const numberOfDays = request.numberOfDays == null ? DEFAULT_QUERY_DAILY_RETURN_DAYS : request.numberOfDays;

    const symbolLastPriceList: IGroupedSymbolDailyResponse[] = await this.symbolDailyRepository
      .queryGroupedSymbolDailyList(request.symbolList, numberOfDays)
      .toArray();

    const codeReturnsDict = {};
    for (const daily of symbolLastPriceList) {
      for (const item of daily.items) {
        if (codeReturnsDict[daily._id] == null) {
          codeReturnsDict[daily._id] = [];
        }
        codeReturnsDict[daily._id].push(item.returns);
      }
    }

    return codeReturnsDict;
  }
}
