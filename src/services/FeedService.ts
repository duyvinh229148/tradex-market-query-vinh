import { Inject, Service } from 'typedi';
import ConfigResponse from '../models/response/ConfigResponse';
import { Errors } from 'tradex-common';
import * as Ajv from 'ajv';
import {
  TradingViewHistoryRequest,
  TradingViewHistoryResponse,
  TradingViewSymbolSearchRequest,
  TradingViewSymbolSearchResponse,
  TradingViewSymbolInfoRequest,
  SymbolPeriodResponse,
} from 'tradex-models-market';
import {
  tradingViewHistoryRequestValidator,
  tradingViewSymbolInfoRequestValidator,
  tradingViewSymbolSearchRequestValidator,
} from 'tradex-models-market-validator';
import { ISymbolInfo } from '../models/db/ISymbolInfo';
import RedisService, { REDIS_KEY } from './RedisService';
import {
  parseSymbolQuoteMinuteList,
  parseTradingviewDailyPeriodList,
  parseSymbolInfo,
  toQuerySymbolSearchResponse,
} from '../utils/ResponseUtils';
import {
  INVALID_PARAMETER,
  RESOLUTION_MINUTE,
  DEFAULT_FLOOR_DATE,
  DEFAULT_CHART_FETCH_COUNT,
  PERIOD_TYPE,
  RESOLUTION_PERIOD_ENUM,
} from '../constants';
import { ISymbolQuoteMinutes } from '../models/db/ISymbolQuoteMinutes';
import { validateRequest } from '../utils/parse';
import CommonService from './common/CommonService';

@Service()
export default class FeedService {
  @Inject()
  private readonly redisService: RedisService;
  @Inject()
  private readonly commonService: CommonService;

  public async queryConfig(): Promise<ConfigResponse> {
    console.log(JSON.stringify(new ConfigResponse()));
    return new ConfigResponse();
  }

  public async queryTradingviewHistory(request: TradingViewHistoryRequest): Promise<TradingViewHistoryResponse> {
    validateRequest(request, tradingViewHistoryRequestValidator);

    if (RESOLUTION_MINUTE.includes(request.resolution)) {
      return this.getQuoteMinuteHistory(request);
    } else {
      return this.getDailyPeriodHistory(request);
    }
  }

  public async getQuoteMinuteHistory(request: TradingViewHistoryRequest): Promise<TradingViewHistoryResponse> {
    //query list, if list empty, return 1 record < fromTime
    const fromTime: Date = new Date(request.from * 1000);
    let toTime: Date = new Date(request.to * 1000);
    if (request.lastTime != null && request.lastTime < request.to) {
      const newToTime = new Date(request.lastTime * 1000);
      newToTime.setSeconds(0);
      newToTime.setSeconds(newToTime.getSeconds() - 1);
      toTime = new Date(newToTime);
    }
    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_CHART_FETCH_COUNT;

    const symbolQuoteMinuteList: ISymbolQuoteMinutes[] = await this.commonService.actualQueryQuoteMinuteThenGrouped(
      request.symbol,
      fromTime,
      toTime,
      fetchCount,
      +request.resolution
    );
    if (symbolQuoteMinuteList.length > 0) {
      return parseSymbolQuoteMinuteList(symbolQuoteMinuteList);
    } else {
      //query normally, but if list = empty, return nextTimeToDate: $lt fromTime date
      //return nextTime of 1 record which $lt: fromTime
      const nextTimeToDate: Date = new Date(fromTime);
      nextTimeToDate.setSeconds(0);
      nextTimeToDate.setSeconds(nextTimeToDate.getSeconds() - 1);
      const nextQuoteMinute: ISymbolQuoteMinutes[] = await this.commonService.actualQueryQuoteMinuteThenGrouped(
        request.symbol,
        DEFAULT_FLOOR_DATE,
        nextTimeToDate,
        1,
        1
      );
      let nextTime: number;
      if (nextQuoteMinute.length === 0) {
        nextTime = null;
      } else {
        const nextTimeDate: Date = nextQuoteMinute[0].date;
        nextTimeDate.setMilliseconds(0);
        nextTimeDate.setSeconds(0);
        nextTime = nextTimeDate.getTime() / 1000;
      }
      return parseSymbolQuoteMinuteList([], nextTime);
    }
  }

  public async getDailyPeriodHistory(request: TradingViewHistoryRequest): Promise<TradingViewHistoryResponse> {
    //query all from mongo, update today record from redis, sort date: -1, return date: 1
    let toTime: Date = new Date(request.to * 1000);
    if (request.lastTime != null && request.lastTime < request.to) {
      const newToTime = new Date(request.lastTime * 1000);
      newToTime.setSeconds(0);
      newToTime.setMinutes(0);
      newToTime.setHours(0);
      newToTime.setMinutes(newToTime.getMinutes() - 1);
      toTime = new Date(newToTime);
    }
    const fetchCount: number = request.fetchCount != null ? request.fetchCount : DEFAULT_CHART_FETCH_COUNT;

    let periodType;
    switch (request.resolution) {
      case RESOLUTION_PERIOD_ENUM.DAILY:
        periodType = PERIOD_TYPE.DAILY;
        break;
      case RESOLUTION_PERIOD_ENUM.WEEKLY:
        periodType = PERIOD_TYPE.WEEKLY;
        break;
      case RESOLUTION_PERIOD_ENUM.MONTHLY:
        periodType = PERIOD_TYPE.MONTHLY;
        break;
      case RESOLUTION_PERIOD_ENUM.SIX_MONTHLY:
        periodType = PERIOD_TYPE.SIX_MONTH;
        break;
      default:
        return parseTradingviewDailyPeriodList([]);
    }
    const symbolPeriodResponseList: SymbolPeriodResponse[] = await this.commonService.actualQuerySymbolPeriod(
      request.symbol,
      periodType,
      fetchCount,
      toTime
    );
    return parseTradingviewDailyPeriodList(symbolPeriodResponseList);
  }

  public async querySymbolSearch(request: TradingViewSymbolSearchRequest): Promise<TradingViewSymbolSearchResponse[]> {
    const validator: Ajv.ValidateFunction = tradingViewSymbolSearchRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const symbolInfoList: ISymbolInfo[] = await this.redisService.hgetall<ISymbolInfo[]>(REDIS_KEY.SYMBOL_INFO);
    if (symbolInfoList.length < 1) {
      return [];
    }

    const result: ISymbolInfo[] = [];
    const priorityResult: ISymbolInfo[] = [];
    let finalResult: ISymbolInfo[];
    for (let i = 0; i < symbolInfoList.length; i++) {
      const symbol: ISymbolInfo = symbolInfoList[i];
      if (request.type != null && request.type.toUpperCase() !== symbol.type.toUpperCase()) {
        continue;
      }

      if (request.exchange != null && request.exchange.toUpperCase() !== symbol.marketType.toUpperCase()) {
        continue;
      }

      if (request.query != null) {
        if (symbol.code.toUpperCase().includes(request.query.toUpperCase())) {
          priorityResult.push(symbol);
          continue;
        }
        if (
          (symbol.name == null || !symbol.name.toUpperCase().includes(request.query.toUpperCase())) &&
          (symbol.nameEn == null || !symbol.nameEn.toUpperCase().includes(request.query.toUpperCase()))
        ) {
          continue;
        }
      }
      result.push(symbol);
    }
    if (request.limit != null) {
      finalResult = priorityResult.concat(result).slice(0, request.limit);
      return finalResult.map(toQuerySymbolSearchResponse);
    }
    finalResult = priorityResult.concat(result);
    return finalResult.map(toQuerySymbolSearchResponse);
  }

  public async querySymbolInfo(request: TradingViewSymbolInfoRequest): Promise<any> {
    const validator: Ajv.ValidateFunction = tradingViewSymbolInfoRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }
    const data = request.symbol.split(':');
    const exchange = (data.length > 1 ? data[0] : '').toUpperCase();
    const code = (data.length > 1 ? data[1] : request.symbol).toUpperCase();

    const symbolInfo: ISymbolInfo = await this.redisService.hget<ISymbolInfo>(REDIS_KEY.SYMBOL_INFO, code);

    if (symbolInfo != null) {
      if (
        symbolInfo.code.toUpperCase() === code &&
        (exchange == null || exchange === '' || exchange === symbolInfo.marketType.toUpperCase())
      ) {
        return parseSymbolInfo(symbolInfo);
      }
    } else {
      return parseSymbolInfo(null);
    }
  }
}
