import * as redis from 'redis';
import config from '../config';
import { Service } from 'typedi';
import { ISymbolInfo } from '../models/db/ISymbolInfo';

const DATA_TYPE = {
  UNDEFINED: 'a',
  NULL: 'b',
  BOOLEAN: '0',
  STRING: '1',
  NUMBER: '2',
  DATE: '3',
  OBJECT: '4',
};

export const REDIS_KEY = {
  SYMBOL_INFO: 'realtime_mapSymbolInfo',
  SYMBOL_DAILY: 'realtime_mapSymbolDaily',
  FOREIGNER_DAILY: 'realtime_mapForeignerDaily',
  SYMBOL_QUOTE: 'realtime_listQuote',
  SYMBOL_BID_OFFER: 'realtime_listBidOffer',
  SYMBOL_QUOTE_MINUTE: 'realtime_listQuoteMinute',
  DEAL_NOTICE: 'realtime_listDealNotice',
  ADVERTISED: 'realtime_listAdvertised',
  MARKET_STATUS: 'realtime_mapMarketStatus',
};

export class KeyNotExist extends Error {
  constructor(key: string) {
    super(`key ${key} is not existed`);
  }
}

@Service()
export default class RedisService {
  private client: redis.RedisClient;

  public async init(): Promise<void> {
    this.client = redis.createClient(config.redis.port, config.redis.host, config.redis.options);
    this.client.on('error', console.error);
    return new Promise((resolve: Function) => {
      this.client.on('ready', () => {
        resolve();
      });
    });
  }

  public receiver(key: string, value: string): any {
    const dateFormat = /^\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}/;
    if (typeof value === 'string' && dateFormat.test(value)) {
      return new Date(value);
    }
    return value;
  }
  public async set<T>(key: string, value: T): Promise<void> {
    let valueAsString = null;
    if (value === null) {
      valueAsString = `${DATA_TYPE.NULL}${value}`;
    } else if (value === undefined) {
      valueAsString = `${DATA_TYPE.UNDEFINED}${value}`;
    } else if (typeof value === 'boolean') {
      valueAsString = `${DATA_TYPE.BOOLEAN}${value ? '1' : '0'}`;
    } else if (typeof value === 'string') {
      valueAsString = `${DATA_TYPE.STRING}${value}`;
    } else if (typeof value === 'number') {
      valueAsString = `${DATA_TYPE.NUMBER}${value}`;
    } else if (value instanceof Date) {
      valueAsString = `${DATA_TYPE.DATE}${((value as unknown) as Date).getTime()}`;
    } else {
      valueAsString = `${DATA_TYPE.OBJECT}${JSON.stringify(value)}`;
    }
    return new Promise((resolve: Function, reject: Function) => {
      this.client.set(key, valueAsString, (err: Error | null) => {
        if (err != null) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
  public async rpush<T>(key: string, value: T): Promise<void> {
    let valueAsString: string = null;
    if (value === null) {
      valueAsString = `${DATA_TYPE.NULL}${value}`;
    } else if (value === undefined) {
      valueAsString = `${DATA_TYPE.UNDEFINED}${value}`;
    } else if (typeof value === 'boolean') {
      valueAsString = `${DATA_TYPE.BOOLEAN}${value ? '1' : '0'}`;
    } else if (typeof value === 'string') {
      valueAsString = `${DATA_TYPE.STRING}${value}`;
    } else if (typeof value === 'number') {
      valueAsString = `${DATA_TYPE.NUMBER}${value}`;
    } else if (value instanceof Date) {
      valueAsString = `${DATA_TYPE.DATE}${((value as unknown) as Date).getTime()}`;
    } else {
      valueAsString = `${DATA_TYPE.OBJECT}${JSON.stringify(value)}`;
    }

    return new Promise((resolve: Function, reject: Function) => {
      this.client.rpush(key, valueAsString, (err: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
  public async hset<T>(key: string, field: string, value: T): Promise<void> {
    let valueAsString = null;
    if (value === null) {
      valueAsString = `${DATA_TYPE.NULL}${value}`;
    } else if (value === undefined) {
      valueAsString = `${DATA_TYPE.UNDEFINED}${value}`;
    } else if (typeof value === 'boolean') {
      valueAsString = `${DATA_TYPE.BOOLEAN}${value ? '1' : '0'}`;
    } else if (typeof value === 'string') {
      valueAsString = `${DATA_TYPE.STRING}${value}`;
    } else if (typeof value === 'number') {
      valueAsString = `${DATA_TYPE.NUMBER}${value}`;
    } else if (value instanceof Date) {
      valueAsString = `${DATA_TYPE.DATE}${((value as unknown) as Date).getTime()}`;
    } else {
      valueAsString = `${DATA_TYPE.OBJECT}${JSON.stringify(value)}`;
    }
    return new Promise((resolve: Function, reject: Function) => {
      this.client.hset(key, field, valueAsString, (err: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve();
        }
      });
    });
  }
  public async lrange<T>(key: string, start: number, end: number): Promise<T[]> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.lrange(key, start, end, (err: any, reply: string[]) => {
        if (err != null) {
          reject(err);
        } else {
          if (reply == null) {
            resolve([]);
          } else {
            const response: any[] = [];
            reply.forEach((item: string) => {
              const type = item[0];
              if (type === DATA_TYPE.NULL) {
                response.push(null);
              } else if (type === DATA_TYPE.UNDEFINED) {
                response.push(undefined);
              } else if (typeof type === DATA_TYPE.BOOLEAN) {
                const content = item.substr(1);
                response.push(((content === '1') as unknown) as T);
              } else if (typeof type === DATA_TYPE.STRING) {
                const content = item.substr(1);
                response.push((content as unknown) as T);
              } else if (typeof type === DATA_TYPE.NUMBER) {
                const content = item.substr(1);
                response.push((Number(content) as unknown) as T);
              } else if (type === DATA_TYPE.DATE) {
                const content = item.substr(1);
                response.push((new Date(Number(content)) as unknown) as T);
              } else {
                const content = item.substr(1);
                response.push(JSON.parse(content, this.receiver));
              }
            });
            resolve(response);
          }
        }
      });
    });
  }
  public async get<T>(key: string): Promise<T> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.get(key, (err: Error | null, reply: string) => {
        if (err != null) {
          reject(err);
        } else {
          if (reply == null) {
            resolve(null);
          } else {
            const type = reply[0];
            if (type === DATA_TYPE.NULL) {
              resolve(null);
            } else if (type === DATA_TYPE.UNDEFINED) {
              resolve(undefined);
            } else if (typeof type === DATA_TYPE.BOOLEAN) {
              const content = reply.substr(1);
              resolve(((content === '1') as unknown) as T);
            } else if (typeof type === DATA_TYPE.STRING) {
              const content = reply.substr(1);
              resolve((content as unknown) as T);
            } else if (typeof type === DATA_TYPE.NUMBER) {
              const content = reply.substr(1);
              resolve((Number(content) as unknown) as T);
            } else if (type === DATA_TYPE.DATE) {
              const content = reply.substr(1);
              resolve((new Date(Number(content)) as unknown) as T);
            } else {
              const content = reply.substr(1);
              resolve(JSON.parse(content, this.receiver));
            }
          }
        }
      });
    });
  }
  public async llen(key: string): Promise<number> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.llen(key, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve(reply);
        }
      });
    });
  }
  public async hlen(key: string): Promise<number> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.hlen(key, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve(reply);
        }
      });
    });
  }
  public async isExists(key: string): Promise<boolean> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.exists(key, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve(reply);
        }
      });
    });
  }
  public async hExists(key: string, field: string): Promise<boolean> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.hexists(key, field, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          resolve(reply);
        }
      });
    });
  }

  public async hget<T>(key: string, field: string): Promise<T> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.hget(key, field, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          if (reply == null) {
            resolve(null);
          } else {
            const type = reply[0];
            if (type === DATA_TYPE.NULL) {
              resolve(null);
            } else if (type === DATA_TYPE.UNDEFINED) {
              resolve(undefined);
            } else if (typeof type === DATA_TYPE.BOOLEAN) {
              const content = reply.substr(1);
              resolve(((content === '1') as unknown) as T);
            } else if (typeof type === DATA_TYPE.STRING) {
              const content = reply.substr(1);
              resolve((content as unknown) as T);
            } else if (typeof type === DATA_TYPE.NUMBER) {
              const content = reply.substr(1);
              resolve((Number(content) as unknown) as T);
            } else if (type === DATA_TYPE.DATE) {
              const content = reply.substr(1);
              resolve((new Date(Number(content)) as unknown) as T);
            } else {
              const content = reply.substr(1);
              resolve(JSON.parse(content, this.receiver));
            }
          }
        }
      });
    });
  }

  public async hgetall<T>(key: string): Promise<T> {
    return new Promise((resolve: Function, reject: Function) => {
      this.client.hgetall(key, (err: any, reply: any) => {
        if (err != null) {
          reject(err);
        } else {
          if (reply == null) {
            resolve([]);
          } else {
            const values: string[] = Object.values(reply);
            const response: any[] = [];
            for (let i = 0; i < values.length; i++) {
              const value: string = values[i];
              const type = value[0];
              if (type === DATA_TYPE.NULL) {
                response.push(null);
              } else if (type === DATA_TYPE.UNDEFINED) {
                response.push(undefined);
              } else if (typeof type === DATA_TYPE.BOOLEAN) {
                const content = value.substr(1);
                response.push(((content === '1') as unknown) as T);
              } else if (typeof type === DATA_TYPE.STRING) {
                const content = value.substr(1);
                response.push((content as unknown) as T);
              } else if (typeof type === DATA_TYPE.NUMBER) {
                const content = value.substr(1);
                response.push((Number(content) as unknown) as T);
              } else if (type === DATA_TYPE.DATE) {
                const content = value.substr(1);
                response.push((new Date(Number(content)) as unknown) as T);
              } else {
                const content = value.substr(1);
                response.push(JSON.parse(content, this.receiver));
              }
            }
            resolve(response);
          }
        }
      });
    });
  }

  public async getSymbolInfo(symbol: string): Promise<ISymbolInfo> {
    return this.hget<ISymbolInfo>(REDIS_KEY.SYMBOL_INFO, symbol);
  }
}
