import { Inject, Service } from 'typedi';
import { TopAiRatingRepository } from '../repositories/TopAiRatingRepository';
import { validateRequest } from '../utils/parse';
import { toTopAiRatingResponse } from '../utils/ResponseUtils';
import { DEFAULT_PAGE_SIZE } from '../constants';
import { FilterQuery } from 'mongodb';
import { ITopAiRating } from '../models/db/ITopAiRating';
import {
	TopAiRatingRequest,
	TopAiRatingResponse,
} from 'tradex-models-market';
import {
	topAiRatingRequestValidator,
} from 'tradex-models-market-validator';
@Service()
export default class TopAiRatingService {
  @Inject()
  private readonly topAiRatingRepository: TopAiRatingRepository;

  public async queryTopAiRating(request: TopAiRatingRequest): Promise<TopAiRatingResponse> {
    validateRequest(request, topAiRatingRequestValidator);

    const fetchCount: number = request.fetchCount == null ? DEFAULT_PAGE_SIZE : request.fetchCount;
    const filter: FilterQuery<ITopAiRating> = {};
    if (request.lastOverAll != null) {
      filter.overall = {
        $lt: request.lastOverAll,
      };
    }

    const recordList = await this.topAiRatingRepository.findBy(filter, fetchCount, 0, { overall: -1 }).toArray();
    return toTopAiRatingResponse(recordList);
  }
}
