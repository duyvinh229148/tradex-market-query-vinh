import { Inject, Service } from 'typedi';
import { Logger, Utils, Kafka, Errors } from 'tradex-common';
import { SecuritiesTypeEnum, DIVIDEND_INFO, DEFAULT_PAGE_SIZE } from '../constants';
import { DividendRepository } from '../repositories/DividendRepository';
import { IDividend } from '../models/db/IDividend';
import { IndexStockListRepository } from '../repositories/IndexStockListRepository';
import { DividendListRequest, DividendListResponse } from 'tradex-models-business-info';
import { v4 as uuid } from 'uuid';
import config from '../config';
import { SymbolQuoteRepository } from '../repositories/SymbolQuoteRepository';
import { SymbolInfoRepository } from '../repositories/SymbolInfoRepository';
import { ISymbolInfo } from '../models/db/ISymbolInfo';
import { IFuturesDailyListHistory } from '../models/db/IFuturesDailyListHistory';
import { FuturesDailyHistoryListRepository } from '../repositories/FuturesDailyHistoryListRepository';
import { IDividendInfo } from '../models/db/IDividenInfo';
import SymbolService from './SymbolService';
import { ISyncIndexStockListResponse } from '../models/response/ISyncIndexStockList';
import { ISymbolDaily } from '../models/db/ISymbolDaily';
import { SymbolDailyRepository } from '../repositories/SymbolDailyRepository';
import { IGroupedSymbolDailyResponse } from '../models/response/IGroupedSymbolDailyResponse';

@Service()
export default class JobService {
  @Inject()
  private symbolQuoteRepository: SymbolQuoteRepository;
  @Inject()
  private symbolInfoRepository: SymbolInfoRepository;
  @Inject()
  public dividendRepository: DividendRepository;
  @Inject()
  public indexStockListRepository: IndexStockListRepository;
  @Inject()
  public futuresDailyHistoryListRepository: FuturesDailyHistoryListRepository;
  @Inject()
  public symbolService: SymbolService;
  @Inject()
  private readonly symbolDailyRepository: SymbolDailyRepository;

  public async storeAutoDataToHistory() {
    this.symbolQuoteRepository.moveToHistory();
  }

  public removeAutoData() {
    this.symbolQuoteRepository
      .deleteAll()
      .then((result: any) => {
        Logger.info(`Delete All SymbolQuote Done: ${result}`);
      })
      .catch((error: any) => Logger.error(error));
    this.symbolInfoRepository
      .resetBidOfferList()
      .then((result: any) => {
        Logger.info(`resetBidOfferList Done: ${result}`);
      })
      .catch((error: any) => Logger.error(error));
  }

  public async storeDailyFuturesList() {
    const dailyFuturesList: ISymbolInfo[] = await this.symbolInfoRepository
      .findBy({ type: SecuritiesTypeEnum.FUTURES }, DEFAULT_PAGE_SIZE, {
        _id: 1,
      })
      .toArray();

    if (dailyFuturesList != null && dailyFuturesList.length > 0) {
      const futuresList: string[] = dailyFuturesList.map((futuresInfo: ISymbolInfo) => futuresInfo._id);
      const nearestExpiredDate: string = Utils.formatDateToDisplay(dailyFuturesList[0].maturityDate);
      const inputData: IFuturesDailyListHistory = {
        _id: Utils.formatDateToDisplay(new Date(), Utils.DATE_DISPLAY_FORMAT),
        nearestExpiredDate: nearestExpiredDate,
        futuresList: futuresList,
      };
      await this.futuresDailyHistoryListRepository.insertDailyFuturesList(inputData);
    }
  }

  public async updateAdjustedPrice() {
    const today: Date = new Date();
    Logger.info(`---------------- updateAdjustedPrice ${today}`);
    try {
      Logger.info(`try to updateDividend before updateAdjustedPrice`);
      await this.updateDividend();
      Logger.info(`updateDividend done!`);
    } catch (e) {
      Logger.error(`error on update dividend ${e}`);
    }

    Logger.info(`start calculate adjusted price:`);
    // query today all dividend info
    const dividendList: IDividend[] = await this.dividendRepository.findBy({
      exDividendDate: {
        $gte: Utils.getStartOfDate(today),
        $lt: Utils.getEndOfDate(today),
      },
    });

    const symbolDividendDict = {};
    for (const dividend of dividendList) {
      if (symbolDividendDict[dividend.code] == null) {
        symbolDividendDict[dividend.code] = [];
      }
      symbolDividendDict[dividend.code].push(dividend);
    }
    Logger.info(`DividendInfo: ${JSON.stringify(symbolDividendDict)}`);

    for (const code of Object.keys(symbolDividendDict)) {
      const dividendList: IDividend[] = symbolDividendDict[code];

      const lastDividendByCode: IDividend = await this.dividendRepository.findLastDividendLtDate(code, today);

      const dividendInfo: IDividendInfo = {
        code: code,
        lastExDividendDate: lastDividendByCode != null ? lastDividendByCode.exDividendDate : null,
      };
      //loop through all the IDividend of that code to form a final IDividendInfo
      for (const dividend of dividendList) {
        if (dividend.eventType === DIVIDEND_INFO.RIGHTS_ISSUE) {
          dividendInfo.ratioIssue = dividend.ratio;
          dividendInfo.priceIssue = dividend.price;
        } else if (dividend.eventType === DIVIDEND_INFO.DIVIDEND) {
          dividendInfo.ratioDividend = dividend.ratio;
        } else if (dividend.eventType === DIVIDEND_INFO.BONUS_SHARE) {
          dividendInfo.ratioBonusShare = dividend.ratio;
        }
      }
      Logger.info(`------------------ Dividend Info: ${JSON.stringify(dividendInfo)}`);
      try {
        await this.symbolService.updateDailyStockByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateDailyStockByDividend', e);
      }
      try {
        await this.symbolService.updateAdjustedPriceForSymbolPrevious(dividendInfo);
      } catch (e) {
        Logger.logError('error while updateAdjustedPriceForSymbolPrevious', e);
      }
      try {
        await this.symbolService.updateMonthlyStockByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateMonthlyStockByDividend', e);
      }
      try {
        await this.symbolService.updateWeeklyStockByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateWeeklyStockByDividend', e);
      }
      try {
        await this.symbolService.updateMinuteStockQuoteByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateMinuteStockQuoteByDividend', e);
      }
      try {
        // backup
        await this.symbolService.updateSymbolMinuteQuoteBackupByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateSymbolMinuteQuoteBackupByDividend', e);
      }
      try {
        // backup
        await this.symbolService.updateSymbolQuoteBackupByDividend(dividendInfo, today);
      } catch (e) {
        Logger.logError('error while updateSymbolQuoteByDividend', e);
      }
      try {
        // update returns
        await this.symbolService.initSymbolDailyReturns({
          symbolList: [code],
          floorDate: Utils.formatDateToDisplay(dividendInfo.lastExDividendDate),
        });
      } catch (e) {
        Logger.logError('error while initSymbolDailyReturns', e);
      }
    }

    Logger.info(`---------------- updateAdjustedPrice done! -------------`);
  }

  public async updateDividend() {
    const dividendListRequest: DividendListRequest = {
      symbol: null,
      date: Utils.formatDateToDisplay(new Date()),
    };

    const message: Kafka.IMessage = await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.businessInfo,
      '/api/v1/queryDividend',
      dividendListRequest,
      10000
    );
    if (message.data.status != null) {
      throw new Errors.GeneralError(message.data.status.code);
    }
    const dividendListResponse: DividendListResponse[] = message.data.data;

    if (dividendListResponse.length > 0) {
      const dividendList: IDividend[] = dividendListResponse.map((value: DividendListResponse) => {
        return {
          _id: value.id,
          code: value.code,
          eventType: value.eventType,
          exDividendDate: Utils.convertStringToDate(value.exDividendDate),
          implementationDate: Utils.convertStringToDate(value.implementationDate),
          price: value.price,
          ratio: value.ratio,
          comment: value.comment,
          updatedAt: new Date(),
        };
      });
      await this.dividendRepository.updateByBulk(dividendList);
    }
  }

  public async syncIndexStockList() {
    const message: Kafka.IMessage = await Kafka.getInstance().sendRequestAsync(
      uuid(),
      config.topic.marketDataCrawler,
      '/api/v1/syncIndexStockList',
      {},
      10000
    );
    if (message.data.status != null) {
      throw new Errors.GeneralError(message.data.status.code);
    }

    const indexStockList: ISyncIndexStockListResponse[] = message.data.data;
    if (indexStockList.length > 0) {
      for (const indexStock of indexStockList) {
        await this.indexStockListRepository.updateIndexStockList(
          { _id: indexStock._id },
          {
            $set: { stockList: indexStock.stockList, updatedAt: new Date() },
          }
        );
      }
    }
  }

  public async updateDailyReturns(): Promise<any> {
    // this job will run after the today's data from redis have been saved to db
    // calculate returns of the just saved daily data

    // get allUnderlyingSymbolList
    const allUnderlyingSymbolList: string[] = await this.symbolService.getAllUnderlyingSymbolList();

    // query today and previous
    const groupedSymbolDailyList: IGroupedSymbolDailyResponse[] = await this.symbolDailyRepository
      .queryGroupedSymbolDailyList(allUnderlyingSymbolList, 2)
      .toArray();

    const finalTodaySymbolDailyList: ISymbolDaily[] = [];
    let isInited = false;
    for (const codeData of groupedSymbolDailyList) {
      // inside a single code
      const todayAndPreviousRecords: ISymbolDaily[] = codeData.items;
      const todayData: ISymbolDaily = todayAndPreviousRecords[0];
      const previousData: ISymbolDaily = todayAndPreviousRecords[1];
      // check if need to init
      if (previousData == null) {
        continue;
      } else {
        if (previousData.returns == null && isInited === false) {
          Logger.info(`==============initSymbolDailyReturns=============`);
          await this.symbolService.initSymbolDailyReturns();
          isInited = true;
        }
      }
      todayData.returns = Math.log(todayData.last / previousData.last);
      finalTodaySymbolDailyList.push(todayData);
    }

    await this.symbolDailyRepository.updateReturnsByBulk(finalTodaySymbolDailyList);
  }
}
