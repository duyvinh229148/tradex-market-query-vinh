import { Inject, Service } from 'typedi';
import { IMarketSessionStatus } from '../models/db/IMarketSessionStatus';
import * as Ajv from 'ajv';
import { Errors, Logger } from 'tradex-common';
import { INVALID_PARAMETER, MarketTypeEnum, SecuritiesTypeEnum } from '../constants';
import { toMarketSessionStatusResponse } from '../utils/ResponseUtils';
import { MarketSessionStatusRequest, MarketSessionStatusResponse } from 'tradex-models-market';
import { marketSessionStatusRequestValidator } from 'tradex-models-market-validator';
import RedisService, { REDIS_KEY } from './RedisService';
import { SymbolPreviousRepository } from '../repositories/SymbolPreviousRepository';
import { ISymbolPrevious } from '../models/db/ISymbolPrevious';
import { FilterQuery } from 'mongodb';

@Service()
export default class MarketSessionStatusService {
  @Inject()
  private redisService: RedisService;
  @Inject()
  private symbolPreviousRepo: SymbolPreviousRepository;

  public async queryMarketSessionStatus(request: MarketSessionStatusRequest): Promise<MarketSessionStatusResponse[]> {
    const validator: Ajv.ValidateFunction = marketSessionStatusRequestValidator();
    if (!validator(request)) {
      throw new Errors.GeneralError(INVALID_PARAMETER, validator.errors);
    }

    const marketSessionStatuses: IMarketSessionStatus[] = await this.redisService.hgetall(REDIS_KEY.MARKET_STATUS);

    if (marketSessionStatuses.length < 1) {
      Logger.info(`redis empty data`);
      return [];
    }
    const mapPrevious = new Map();
    const query: FilterQuery<ISymbolPrevious> = {
      marketType: MarketTypeEnum.HOSE,
      type: SecuritiesTypeEnum.STOCK,
      previousTradingDate: { $ne: null },
    };
    const sort: any = {
      lastTradingDate: -1,
      previousTradingDate: -1,
    };
    const hoseEquityPreviousList: ISymbolPrevious[] = await this.symbolPreviousRepo.findBy(query, 1, 0, sort).toArray();
    mapPrevious.set('HOSE_EQUITY', hoseEquityPreviousList[0]);
    Logger.error('HOSE_EQUITY', JSON.stringify(hoseEquityPreviousList[0]));

    query.marketType = MarketTypeEnum.HNX;
    const hnxEquityPreviousList: ISymbolPrevious[] = await this.symbolPreviousRepo.findBy(query, 1, 0, sort).toArray();
    mapPrevious.set('HNX_EQUITY', hnxEquityPreviousList[0]);

    query.marketType = MarketTypeEnum.UPCOM;
    const upComEquityPreviousList: ISymbolPrevious[] = await this.symbolPreviousRepo
      .findBy(query, 1, 0, sort)
      .toArray();
    mapPrevious.set('UPCOM_EQUITY', upComEquityPreviousList[0]);

    query.marketType = MarketTypeEnum.HNX;
    query.type = SecuritiesTypeEnum.FUTURES;
    const hnxDerivativesPreviousList: ISymbolPrevious[] = await this.symbolPreviousRepo
      .findBy(query, 1, 0, sort)
      .toArray();
    mapPrevious.set('HNX_DERIVATIVES', hnxDerivativesPreviousList[0]);

    const response: MarketSessionStatusResponse[] = [];
    for (const value of marketSessionStatuses) {
      if (
        (request.market == null || request.market === MarketTypeEnum.ALL || value.market === request.market) &&
        (request.type == null || value.type === request.type)
      ) {
        response.push(toMarketSessionStatusResponse(value, mapPrevious.get(`${value.market}_${value.type}`)));
      }
    }
    return response;
  }
}
