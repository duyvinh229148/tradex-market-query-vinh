export interface IChart {
  _id?: string;
  ownerSource?: string;
  ownerId?: string;
  content?: string;
  name?: string;
  symbol?: string;
  resolution?: string;
  lastModified?: Date;
}
