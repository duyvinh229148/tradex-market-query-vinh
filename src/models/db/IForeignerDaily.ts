export interface IForeignerDaily {
  _id?: string;
  code?: string;
  foreignerBuyVolume?: number;
  foreignerSellVolume?: number;
  foreignerTotalRoom?: number;
  foreignerCurrentRoom?: number;
  foreignerBuyAbleRatio?: number;
  foreignerChangeVolume?: number;
  foreignerHoldVolume?: number;
  foreignerHoldRatio?: number;
  foreignerTradingVolume?: number;
  date?: Date;
}
