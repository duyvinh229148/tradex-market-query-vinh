export interface IEtfNavDaily {
  _id?: string;
  code?: string;
  date?: Date;
  open?: number;
  high?: number;
  low?: number;
  last?: number;
  change?: number;
  rate?: number;
  teValue?: number;
  teRate?: number;
}
