export interface IDividend {
  _id?: string;
  code?: string;
  eventType?: string;
  exDividendDate?: Date;
  implementationDate?: Date;
  price?: number;
  ratio?: number;
  comment?: string;
  updatedAt?: Date;
}
