export interface IMarketSessionStatus {
  _id?: string;
  market?: string;
  status?: string;
  date?: Date;
  time?: string;
  type?: string;
  support?: string;
  title?: string;
}
