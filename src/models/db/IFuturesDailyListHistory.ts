export interface IFuturesDailyListHistory {
  _id?: string;
  nearestExpiredDate?: string;
  futuresList?: string[];
}
