export interface IDividendInfo {
  code?: string;
  eventType?: string;
  lastExDividendDate?: Date;
  implementationDate?: Date;
  priceIssue?: number;
  ratioIssue?: number;
  ratioBonusShare?: number;
  ratioDividend?: number;
}
