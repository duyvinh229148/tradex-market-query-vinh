export interface ISyncIndexStockListResponse {
  _id: string;
  stockList: string[];
}
