/* tslint:disable */
import { v4 as uuid } from 'uuid';

const nodeId = uuid();
let config = {
  db: {
    client: 'mongodb',
    connection: {
      url: 'mongodb://localhost:27017/tradex-market',
      database: 'tradex-market',
    },
  },
  redis: {
    port: 6379,
    host: 'localhost',
    options: {},
  },
  logger: {
    config: {
      appenders: {
        application: { type: 'console' },
        file: {
          type: 'file',
          filename: '/logs/application.log',
          compression: true,
          maxLogSize: 10485760,
          backups: 10,
        },
      },
      categories: {
        default: { appenders: ['application', 'file'], level: 'info' },
      },
    },
  },
  topic: {
    marketJob: 'marketJob-v2',
    businessInfo: 'business-info',
    marketDataCrawler: 'market-data-crawler',
  },
  log: {
    serviceName: 'market-service',
    format: 'FLAT', // 'FLAT' or 'JSON'
    transport: [],
  },
  clusterId: 'market-v2',
  clientId: `market-${nodeId}`,
  nodeId: nodeId,
  kafkaUrls: ['localhost:9092'],
  kafkaCommonOptions: {},
  kafkaConsumerOptions: {},
  kafkaProducerOptions: {},
  kafkaTopicOptions: {},
  zkUrls: ['localhost:2181'],
};

try {
  const env = require('./env');
  if (env) {
    config = { ...config, ...env(config) };
  }
} catch (e) {
  //swalow it
}

config.kafkaConsumerOptions = {
  ...(config.kafkaCommonOptions ? config.kafkaCommonOptions : {}),
  ...(config.kafkaConsumerOptions ? config.kafkaConsumerOptions : {}),
};
config.kafkaProducerOptions = {
  ...(config.kafkaCommonOptions ? config.kafkaCommonOptions : {}),
  ...(config.kafkaProducerOptions ? config.kafkaProducerOptions : {}),
};

export default config;
