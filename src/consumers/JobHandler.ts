import { Inject, Service } from 'typedi';
import config from '../config';
import { Errors, Kafka } from 'tradex-common';
import JobService from '../services/JobService';

@Service()
export default class JobHandler {
  @Inject()
  private jobService: JobService;

  public init() {
    const handle: Kafka.MessageHandler = new Kafka.MessageHandler();
    new Kafka.StreamHandler(
      config,
      config.kafkaConsumerOptions,
      [config.topic.marketJob],
      (message: any) => handle.handle(message, this.handleRequest),
      config.kafkaTopicOptions
    );
  }

  private handleRequest: Kafka.Handle = (message: Kafka.IMessage) => {
    if (message == null) {
      return Promise.reject(new Errors.SystemError());
    }
    if (message.uri === '/job/removeAutoData') {
      this.jobService.removeAutoData();
      return true;
    } else if (message.uri === '/job/storeAutoData') {
      return this.jobService.storeAutoDataToHistory();
    } else if (message.uri === '/job/storeDailyFuturesList') {
      return this.jobService.storeDailyFuturesList();
    } else if (message.uri === '/job/updateAdjustedPrice') {
      return this.jobService.updateAdjustedPrice();
    } else if (message.uri === '/job/updateDividend') {
      return this.jobService.updateDividend();
    } else if (message.uri === '/job/syncIndexStockList') {
      return this.jobService.syncIndexStockList();
    } else if (message.uri === '/job/updateDailyReturns') {
      return this.jobService.updateDailyReturns();
    }
    return false;
  };
}
