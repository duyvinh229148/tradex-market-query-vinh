import { Inject, Service } from 'typedi';
import config from '../config';
import { Errors, Kafka, ServiceRegistration, Utils } from 'tradex-common';
import SymbolService from '../services/SymbolService';
import MarketSessionStatusService from '../services/MarketSessionStatusService';
import EtfService from '../services/EtfService';
import PutThroughService from '../services/PutThroughService';
import FixService from '../services/FixService';
import FeedService from '../services/FeedService';
import ChartService from '../services/ChartService';
import TopAiRatingService from '../services/TopAiRatingService';

@Service()
export default class RequestHandler {
  @Inject()
  private symbolService: SymbolService;
  @Inject()
  private marketSessionStatus: MarketSessionStatusService;
  @Inject()
  private etfService: EtfService;
  @Inject()
  private putThroughService: PutThroughService;
  @Inject()
  private fixService: FixService;
  @Inject()
  private feedService: FeedService;
  @Inject()
  private chartService: ChartService;
  @Inject()
  private topAiRatingService: TopAiRatingService;

  public init() {
    ServiceRegistration.create(Kafka.getInstance(), {
      nodeId: config.nodeId,
      serviceName: config.clusterId,
    });
    const handle: Kafka.MessageHandler = new Kafka.MessageHandler();
    new Kafka.StreamHandler(
      config,
      config.kafkaConsumerOptions,
      [config.clusterId],
      (message: any) => handle.handle(message, this.handleRequest),
      config.kafkaTopicOptions
    );
  }

  private handleRequest: Kafka.Handle = (message: Kafka.IMessage) => {
    if (message == null || message.data == null) {
      return Promise.reject(new Errors.SystemError());
    } else {
      if (message.uri === '/api/v2/market/symbol') {
        return true;
      }
      if (message.uri === '/api/v2/market/symbol/latest') {
        return this.symbolService.querySymbolLatest(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/staticInfo') {
        return this.symbolService.querySymbolStaticInfo(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/{symbol}/quote') {
        return this.symbolService.querySymbolQuote(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/{symbol}/period/{periodType}') {
        return this.symbolService.querySymbolPeriod(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/{symbol}/ticks') {
        return this.symbolService.querySymbolQuoteTick(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/{symbol}/minutes') {
        return this.symbolService.querySymbolQuoteMinutes(message.data);
      }
      if (message.uri === '/api/v2/market/sessionStatus') {
        return this.marketSessionStatus.queryMarketSessionStatus(message.data);
      }
      if (message.uri === '/api/v2/market/etf/{symbolCode}/nav/daily') {
        return this.etfService.queryEtfNavDaily(message.data);
      }
      if (message.uri === '/api/v2/market/etf/{symbolCode}/index/daily') {
        return this.etfService.queryEtfIndexDaily(message.data);
      }
      if (message.uri === '/api/v2/market/symbol/{symbolCode}/foreigner') {
        return this.symbolService.querySymbolForeignerDaily(message.data);
      }
      if (message.uri === '/api/v2/market/ranking/{symbolType}/trade') {
        return this.symbolService.querySymbolRankingTrade(message.data);
      } else if (message.uri === '/api/v2/market/putthrough/advertise') {
        return this.putThroughService.queryPutThroughAdvertise(message.data);
      } else if (message.uri === '/api/v2/market/putthrough/deal') {
        return this.putThroughService.queryPutThroughDeal(message.data);
      } else if (message.uri === '/api/v2/market/stock/ranking/upDown') {
        return this.symbolService.queryStockRankingUpDown(message.data);
      } else if (message.uri === '/api/v2/market/stock/ranking/top') {
        return this.symbolService.queryStockRankingTop(message.data);
      } else if (message.uri === '/api/v2/market/topForeignerTrading') {
        return this.symbolService.queryTopForeignerTrading(message.data);
      } else if (message.uri === '/api/v2/market/indexStockList') {
        return this.symbolService.queryIndexStockList(message.data);
      } else if (message.uri === '/api/v2/market/dailyReturns') {
        return this.symbolService.querySymbolDailyReturns(message.data);
      } else if (message.uri === '/api/v2/market/symbol/tickSizeMatch') {
        return this.symbolService.querySymbolTickSizeMatch(message.data);
      }
      // Fix
      else if (message.uri === '/api/v2/fix/securitiesList') {
        return this.fixService.queryFixSymbolList(message.data);
      }
      // trading view
      else if (message.uri === '/api/v2/tradingview/config') {
        return this.feedService.queryConfig();
      } else if (message.uri === '/api/v2/tradingview/symbols') {
        return this.feedService.querySymbolInfo(message.data);
      } else if (message.uri === '/api/v2/tradingview/search') {
        return this.feedService.querySymbolSearch(message.data);
      } else if (message.uri === '/api/v2/tradingview/history') {
        return this.feedService.queryTradingviewHistory(message.data);
      }
      // Save load charts
      else if (message.uri === '/api/v2/tradingview/charts/save') {
        if (!Utils.isEmpty(message.data.chart)) {
          return this.chartService.updateChart(message.data);
        } else {
          return this.chartService.saveChart(message.data);
        }
      } else if (message.uri === '/api/v2/tradingview/charts/load') {
        if (!Utils.isEmpty(message.data.chart)) {
          return this.chartService.loadChart(message.data);
        } else {
          return this.chartService.listChart(message.data);
        }
      } else if (message.uri === '/api/v2/tradingview/charts/delete') {
        return this.chartService.deleteChart(message.data);
      } else if (message.uri === '/api/v2/market/liquidity') {
        return this.chartService.queryMarketLiquidity(message.data);
      }
      // init symbol daily returns
      else if (message.uri === '/api/v2/market/dailyReturns/init') {
        return this.symbolService.initSymbolDailyReturns(message.data);
      } else if (message.uri === '/api/v2/market/topAiRating') {
        return this.topAiRatingService.queryTopAiRating();
      }
    }
    return false;
  };
}
